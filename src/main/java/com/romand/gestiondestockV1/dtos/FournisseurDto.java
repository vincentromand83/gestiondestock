package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FournisseurDto {

    private String idFournisseur;

    @NotEmpty(message = "Veuillez renseigner le nom du fournisseur")
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prenom du fournisseur")
    private String prenom;

    @NotNull(message = "Veuillez renseigner l'adresse du fournisseur")
    private AdresseDto adresse;

    @Email(message = "L'email du fournisseur doit etre valide")
    private String email;

    @NotEmpty(message = "Veuillez renseigner le numero de telephone du fournisseur")
    @Size(min = 10, max = 25)
    private String numeroTelephone;

    private String photo;
}
