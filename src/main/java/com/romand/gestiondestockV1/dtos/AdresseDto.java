package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdresseDto {

    @NotEmpty(message = "Veuillez renseigner l'adresse 1")
    @Size(min = 5, max = 150)
    private String adresse1;

    private String adresse2;

    @NotEmpty(message = "Veuillez renseigner le code postal de l'adresse")
    @Size(min = 5, max = 20)
    private String codePostal;

    @NotEmpty(message = "Veuillez renseigner la ville de l'adresse")
    @Size(min = 2, max = 100)
    private String ville;

    @NotEmpty(message = "Veuillez renseigner le pays de l'adresse")
    @Size(min = 2, max = 50)
    private String pays;
}
