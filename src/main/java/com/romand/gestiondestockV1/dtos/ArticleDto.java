package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArticleDto {

    private String idArticle;

    @NotEmpty(message = "Veuillez renseigner le code de l'article")
    @Size(min = 4)
    private String codeArticle;

    @NotEmpty(message = "Veuillez renseigner le nom de l'article")
    @Size(min = 4, max = 60, message = "Le nom de l'article doit comporter au moins 4 caractères")
    private String nom;

    @NotNull(message = "Veuillez renseigner le prix unitaire HT de l'article")
    private BigDecimal prixUnitaireHt;

    @NotNull(message = "Veuillez renseigner le taux de TVA de l'article")
    private BigDecimal tauxTva;

    @NotNull(message = "Veuillez renseigner le prix unitaire TTC de l'article")
    private BigDecimal prixUnitaireTtc;

    private String photo;

    @NotNull(message = "Veuillez renseigner la categorie de l'article")
    private CategoryDto category;

    @NotNull(message = "Veuillez renseigner l'entreprise qui posséde l'article")
    private EntrepriseDto entreprise;
}
