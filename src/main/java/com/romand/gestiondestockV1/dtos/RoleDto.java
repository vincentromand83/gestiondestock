package com.romand.gestiondestockV1.dtos;

import com.romand.gestiondestockV1.entities.Utilisateur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleDto {

    private String idRole;

    @NotEmpty(message = "Veuillez renseigner le role name")
    private String roleName;
}
