package com.romand.gestiondestockV1.dtos;

import com.romand.gestiondestockV1.entities.EtatCommande;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommandeFournisseurDto {

    private String idCommandeFournisseur;

    @NotEmpty(message = "Veuillez renseigner le code de la commande fournisseur")
    private String code;

    @NotNull(message = "Veuillez renseigner la date de la commande fournisseur")
    private Date dateCommande;

    @NotNull(message = "Veuillez renseigner l'etat de la commande fournisseur")
    private EtatCommande etatCommande;

    @NotNull(message = "Veuillez renseigner le fournisseur de la commande fournisseur")
    private FournisseurDto fournisseur;
}
