package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LigneVenteDto {

    private String idLigneVente;

    @NotNull(message = "Veuillez renseigner la quantité de la ligne de vente")
    private BigDecimal quantite;

    @NotNull(message = "Veuillez renseigner le prix de la vente")
    private BigDecimal prix;

    @NotNull(message = "Veuillez renseigner l'article vendu dans la ligne de vente")
    private ArticleDto article;

    @NotNull(message = "Veuillez renseigner la vente")
    private VenteDto vente;
}
