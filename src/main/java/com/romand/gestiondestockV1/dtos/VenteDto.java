package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VenteDto {

    private String idVente;

    @NotEmpty(message = "Veuillez renseigner le code de vente")
    private String code;

    @NotNull(message = "Veuillez renseigner la date de la vente")
    private Date dateVente;

    private String commentaire;
}
