package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EntrepriseDto {

    private String idEntreprise;

    @NotEmpty(message = "Veuillez renseigner le nom de l'entreprise")
    private String nom;

    private String description;

    @NotNull(message = "Veuillez renseigner l'adresse de l'entreprise")
    private AdresseDto adresse;

    @NotEmpty(message = "Veuillez renseigner le code fiscal de l'entreprise")
    private String codeFiscal;

    @Email(message = "L'email de l'entreprise doit être valide")
    private String email;

    @NotEmpty(message = "Veuillez renseigner le numero de telephone de l'entreprise")
    @Size(min = 10, max = 25)
    private String numeroTelephone;

    private String photo;
}
