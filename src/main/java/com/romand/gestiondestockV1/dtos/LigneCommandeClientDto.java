package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LigneCommandeClientDto {

    private String idLigneCommandeClient;

    @NotNull(message = "Veuillez renseigner la quantité de la commande client")
    private BigDecimal quantite;

    @NotNull(message = "Veuillez renseigner le prix de la commande client")
    private BigDecimal prix;

    @NotNull(message = "Veuillez renseigner l'Article de la commande client")
    private ArticleDto article;

    @NotNull(message = "Veuillez renseigner la commande client")
    private CommandeClientDto commandeClient;
}
