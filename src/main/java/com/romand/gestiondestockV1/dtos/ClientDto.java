package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientDto {

    private String idClient;

    @NotEmpty(message = "Veuillez renseigner le nom du client")
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prénom du client")
    private String prenom;

    @NotNull(message = "Veuillez renseigner l'adresse du client")
    private AdresseDto adresse;

    @Email(message = "L'Email client doit être valide")
    private String email;

    @NotEmpty(message = "Veuillez renseigner le numero de telephone du client")
    @Size(min = 10, max = 25)
    private String numeroTelephone;

    private String photo;
}
