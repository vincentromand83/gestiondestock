package com.romand.gestiondestockV1.dtos;

import com.romand.gestiondestockV1.entities.SourceMouvementStock;
import com.romand.gestiondestockV1.entities.TypeMouvementStock;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MouvementStockDto {

    private String idMouvementStock;

    private SourceMouvementStock sourceMouvementStock;

    @NotNull(message = "Veuillez renseigner le type de mouvement de stock")
    private TypeMouvementStock typeMouvementStock;

    @NotNull(message = "Veuillez renseigner la date du mouvement de stock")
    private Date dateMouvement;

    @NotNull(message = "Veuillez renseigner la quantité du mouvement de stock")
    private BigDecimal quantite;

    @NotNull(message = "Veuillez renseigner l'article concerne par le mouvement de stock")
    private ArticleDto article;
}
