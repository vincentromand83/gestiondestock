package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDto {

    private String idCategory;

    @NotEmpty(message = "Veuillez renseigner le code de la catégorie")
    @Size(min =  4, max = 20, message = "Le code de la catégorie doit comporter au moins 4 caractères")
    private String code;

    @NotEmpty(message = "Veuillez renseigner le nom de la catégorie")
    @Size(min = 5, max = 60, message = "Le nom de la catégorie doit comporter au moins 5 caractères")
    private String nom;
}
