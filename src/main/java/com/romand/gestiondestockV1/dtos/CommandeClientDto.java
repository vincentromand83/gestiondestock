package com.romand.gestiondestockV1.dtos;

import com.romand.gestiondestockV1.entities.EtatCommande;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommandeClientDto {

    private String idCommandeClient;

    @NotEmpty(message = "Veuillez renseigner le code de la commande client")
    private String code;

    @NotNull(message = "Veuillez reseigner la date de la commande client")
    private Date dateCommande;

    @NotNull(message = "Veuillez renseigner l'etat de la commande")
    private EtatCommande etatCommande;

    @NotNull(message = "Veuillez renseigner le client qui passe la commande")
    private ClientDto client;
}
