package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LigneCommandeFournisseurDto {

    private String idLigneCommandeFournisseur;

    @NotNull(message = "Veuillez renseigner la quantité de la commande fournisseur")
    private BigDecimal quantite;

    @NotNull(message = "Veuillez renseigner le prix de la commande fournisseur")
    private BigDecimal prix;

    @NotNull(message = "Veuillez renseigner l'article de la commande fournisseur")
    private ArticleDto article;

    @NotNull(message = "Veuillez renseigner la commande fournisseur")
    private CommandeFournisseurDto commandeFournisseur;
}
