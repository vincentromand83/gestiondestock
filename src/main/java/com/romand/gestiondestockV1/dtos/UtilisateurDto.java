package com.romand.gestiondestockV1.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UtilisateurDto {

    private String idUtilisateur;

    @NotEmpty(message = "Veuillez renseigner le nom de l'utilisateur")
    @Size(min = 4, max = 60)
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prenom de l'utilisateur")
    private String prenom;

    @NotEmpty(message = "Veuillez renseigner la date de naissance de l'utilisateur")
    private String dateNaissance;

    @Email(message = "L'Email doit etre valide")
    private String email;

    @Size(min = 12, message = "le mot de passe doit contenir au minimum 12 caractères")
    private String password;

    @NotNull(message = "Veuillez renseigner l'adresse de l'utilisateur")
    private AdresseDto adresse;

    private String photo;

    private EntrepriseDto entreprise;

    private List<RoleDto> roles;
}
