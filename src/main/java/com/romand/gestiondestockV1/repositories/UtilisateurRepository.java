package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Utilisateur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, String> {

    Page<Utilisateur> findByEntrepriseIdEntreprise(String idUtilisateur, Pageable pageable);

    List<Utilisateur> findByEntrepriseIdEntreprise(String idEntreprise);

    Optional<Utilisateur> findByEmail(String email);
}
