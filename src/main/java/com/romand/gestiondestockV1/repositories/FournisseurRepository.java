package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FournisseurRepository extends JpaRepository<Fournisseur, String> {
}
