package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.LigneVente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LigneVenteRepository extends JpaRepository<LigneVente, String> {

    Page<LigneVente> findByArticleIdArticle(String idArtcle, Pageable pageable);

    List<LigneVente> findByArticleIdArticle(String idArticle);

    Page<LigneVente> findByVenteIdVente(String idVente, Pageable pageable);

    List<LigneVente> findByVenteIdVente(String idVente);
}
