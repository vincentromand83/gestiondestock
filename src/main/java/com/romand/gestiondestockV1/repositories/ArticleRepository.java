package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, String> {

    Page<Article> findByCategoryIdCategory(String idCategory, Pageable pageable);

    List<Article> findByCategoryIdCategory(String idCategory);

    @Query("SELECT a FROM Article a JOIN Entreprise e ON e.idEntreprise = a.entreprise")
    Page<Article> findByEntrepriseIdEntreprise(String idEntreprise, Pageable pageable);

    List<Article> findByEntrepriseIdEntreprise(String idEntreprise);
}
