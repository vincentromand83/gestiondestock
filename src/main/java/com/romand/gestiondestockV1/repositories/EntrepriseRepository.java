package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Entreprise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrepriseRepository extends JpaRepository<Entreprise, String> {
}
