package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, String> {
}
