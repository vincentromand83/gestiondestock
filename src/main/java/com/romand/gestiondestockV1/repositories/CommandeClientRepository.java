package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import com.romand.gestiondestockV1.entities.CommandeClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommandeClientRepository extends JpaRepository<CommandeClient, String> {

    List<CommandeClient> findByClientIdClient(String idClient);

    Page<CommandeClient> findByClientIdClient(String idClient, Pageable pageable);
}
