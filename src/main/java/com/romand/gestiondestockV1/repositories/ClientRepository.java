package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, String> {
}
