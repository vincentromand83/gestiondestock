package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.LigneCommandeClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LigneCommandeClientRepository extends JpaRepository<LigneCommandeClient, String> {

    Page<LigneCommandeClient> findByArticleIdArticle(String idArticle, Pageable pageable);

    List<LigneCommandeClient> findByArticleIdArticle(String idArticle);

    Page<LigneCommandeClient> findByCommandeClientIdCommandeClient(String idCommandeClient, Pageable pageable);

    List<LigneCommandeClient> findByCommandeClientIdCommandeClient(String idCommandeClient);
}
