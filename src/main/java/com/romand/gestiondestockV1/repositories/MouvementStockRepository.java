package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.MouvementStock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MouvementStockRepository extends JpaRepository<MouvementStock, String> {

    List<MouvementStock> findByArticleIdArticle(String idArticle);

    Page<MouvementStock> findByArticleIdArticle(String idArticle, Pageable pageable);
}
