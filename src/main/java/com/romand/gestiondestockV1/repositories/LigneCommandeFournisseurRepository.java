package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.LigneCommandeFournisseur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LigneCommandeFournisseurRepository extends JpaRepository<LigneCommandeFournisseur, String> {

    Page<LigneCommandeFournisseur> findByArticleIdArticle(String idArticle, Pageable pageable);

    List<LigneCommandeFournisseur> findByArticleIdArticle(String idArticle);

    Page<LigneCommandeFournisseur> findByCommandeFournisseurIdCommandeFournisseur(String idCommandeFournisseur, Pageable pageable);

    List<LigneCommandeFournisseur> findByCommandeFournisseurIdCommandeFournisseur(String idCommandeFournisseur);
}
