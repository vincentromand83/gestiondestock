package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.CommandeFournisseur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommandeFournisseurRepository extends JpaRepository<CommandeFournisseur, String> {
    Page<CommandeFournisseur> findByFournisseurIdFournisseur(String idFournisseur, Pageable pageable);

    List<CommandeFournisseur> findByFournisseurIdFournisseur(String idFournisseur);
}
