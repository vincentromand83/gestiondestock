package com.romand.gestiondestockV1.repositories;

import com.romand.gestiondestockV1.entities.Vente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VenteRepository extends JpaRepository<Vente, String> {
}
