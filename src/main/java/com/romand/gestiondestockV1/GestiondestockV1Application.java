package com.romand.gestiondestockV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication
@EnableJpaAuditing
public class GestiondestockV1Application {

	public static void main(String[] args) {
		SpringApplication.run(GestiondestockV1Application.class, args);
	}
}
