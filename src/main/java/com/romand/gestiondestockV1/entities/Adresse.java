package com.romand.gestiondestockV1.entities;

import lombok.*;
import jakarta.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Embeddable
public class Adresse implements Serializable {

    private String adresse1;

    private String adresse2;

    private String codePostal;

    private String ville;

    private String pays;
}
