package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ligne_vente")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idLigneVente")
public class LigneVente extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idLigneVente;

    @Column(name = "quantite", nullable = false)
    private BigDecimal quantite;

    @Column(name = "prix", nullable = false)
    private BigDecimal prix;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idArticle")
    @ToString.Exclude
    private Article article;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idVente")
    @ToString.Exclude
    private Vente vente;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        LigneVente that = (LigneVente) o;
        return idLigneVente != null && Objects.equals(idLigneVente, that.idLigneVente);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
