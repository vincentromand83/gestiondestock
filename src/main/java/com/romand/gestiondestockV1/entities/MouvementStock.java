package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mouvement_stock")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idMouvementStock")
public class MouvementStock extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idMouvementStock;

    @Column(name = "type_mouvement_stock", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeMouvementStock typeMouvementStock;

    @Column(name = "source_mouvement_stock")
    @Enumerated(EnumType.STRING)
    private SourceMouvementStock sourceMouvementStock;

    @Column(name = "date_mouvement", nullable = false)
    private Date dateMouvement;

    @Column(name = "quantite", nullable = false)
    private BigDecimal quantite;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idArticle")
    @ToString.Exclude
    private Article article;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MouvementStock that = (MouvementStock) o;
        return idMouvementStock != null && Objects.equals(idMouvementStock, that.idMouvementStock);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
