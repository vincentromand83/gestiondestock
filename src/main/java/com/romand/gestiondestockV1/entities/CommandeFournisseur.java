package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "commande_fournisseur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idCommandeFournisseur")
public class CommandeFournisseur extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idCommandeFournisseur;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "date_commande", nullable = false)
    private Date dateCommande;

    @Column(name = "etat_commande", nullable = false)
    @Enumerated(EnumType.STRING)
    private EtatCommande etatCommande;

    @OneToMany(mappedBy = "commandeFournisseur")
    @ToString.Exclude
    private List<LigneCommandeFournisseur> ligneCommandeFournisseurs;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idFournisseur")
    @ToString.Exclude
    private Fournisseur fournisseur;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CommandeFournisseur that = (CommandeFournisseur) o;
        return idCommandeFournisseur != null && Objects.equals(idCommandeFournisseur, that.idCommandeFournisseur);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
