package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "article")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idArticle")
public class Article extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idArticle;

    @Column(name = "code_article", nullable = false)
    private String codeArticle;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prix_unitaire_ht", nullable = false)
    private BigDecimal prixUnitaireHt;

    @Column(name = "taux_tva", nullable = false)
    private BigDecimal tauxTva;

    @Column(name = "prix_unitaire_ttc", nullable = false)
    private BigDecimal prixUnitaireTtc;

    @Column(name = "photo")
    private String photo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idCategory")
    @ToString.Exclude
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idEntreprise")
    @ToString.Exclude
    private Entreprise entreprise;

    @OneToMany(mappedBy = "article")
    @ToString.Exclude
    private List<MouvementStock> mouvementStocks;

    @OneToMany(mappedBy = "article")
    @ToString.Exclude
    private List<LigneVente> ligneVentes;

    @OneToMany(mappedBy = "article")
    @ToString.Exclude
    private List<LigneCommandeClient> ligneCommandeClients;

    @OneToMany(mappedBy = "article")
    @ToString.Exclude
    private List<LigneCommandeFournisseur> ligneCommandeFournisseurs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Article article = (Article) o;
        return idArticle != null && Objects.equals(idArticle, article.idArticle);
    }


    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
