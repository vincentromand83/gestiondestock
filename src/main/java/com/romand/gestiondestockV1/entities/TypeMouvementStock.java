package com.romand.gestiondestockV1.entities;

public enum TypeMouvementStock {

    ENTREE,
    SORTIE,
    CORRECTION_POSITIVE,
    CORRECTION_NEGATIVE
}
