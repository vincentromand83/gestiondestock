package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vente")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idVente")
public class Vente extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idVente;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "date_vente", nullable = false)
    private Date dateVente;

    @Column(name = "commentaire")
    private String commentaire;

    @OneToMany(mappedBy = "vente")
    @ToString.Exclude
    private List<LigneVente> ligneVentes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Vente vente = (Vente) o;
        return idVente != null && Objects.equals(idVente, vente.idVente);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
