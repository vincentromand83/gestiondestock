package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "commande_client")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idCommandeClient")
public class CommandeClient extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idCommandeClient;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "date_commande", nullable = false)
    private Date dateCommande;

    @Column(name = "etat_commande", nullable = false)
    @Enumerated(EnumType.STRING)
    private EtatCommande etatCommande;

    @OneToMany(mappedBy = "commandeClient")
    @ToString.Exclude
    private List<LigneCommandeClient> ligneCommandeClients;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idClient")
    @ToString.Exclude
    private Client client;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CommandeClient that = (CommandeClient) o;
        return idCommandeClient != null && Objects.equals(idCommandeClient, that.idCommandeClient);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
