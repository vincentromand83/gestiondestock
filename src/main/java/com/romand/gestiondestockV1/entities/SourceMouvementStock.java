package com.romand.gestiondestockV1.entities;

public enum SourceMouvementStock {

    COMMANDE_CLIENT,
    COMMANDE_FOURNISSEUR,
    VENTE
}
