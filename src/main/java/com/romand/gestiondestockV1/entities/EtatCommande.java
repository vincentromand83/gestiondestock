package com.romand.gestiondestockV1.entities;

public enum EtatCommande {

    EN_PREPARATION,
    VALIDEE,
    LIVREE
}
