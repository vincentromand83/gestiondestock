package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "fournisseur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idForunisseur")
public class Fournisseur extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idFournisseur;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Embedded
    private Adresse adresse;

    @Column(name = "photo")
    private String photo;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "numero_telephone", nullable = false)
    private String numeroTelephone;

    @OneToMany(mappedBy = "fournisseur")
    @ToString.Exclude
    private List<CommandeFournisseur> commandeFournisseurs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Fournisseur fournisseur = (Fournisseur) o;
        return idFournisseur != null && Objects.equals(idFournisseur, fournisseur.idFournisseur);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}