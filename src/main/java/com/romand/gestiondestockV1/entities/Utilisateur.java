package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "utilisateur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idUtilisateur")
public class Utilisateur extends AbstractEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idUtilisateur;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Column(name = "date_naissance")
    private String dateNaissance;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false, unique = true)
    private String password;

/*    @Embedded
    private Adresse adresse;*/

    @Column(name = "photo")
    private String photo;

    @ManyToMany
    @JoinTable(name = "utilisateur_role",
    joinColumns = @JoinColumn(name = "utilisateur_id", referencedColumnName = "idUtilisateur"),
    inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "idRole"))
    @ToString.Exclude
    private List<Role> roles;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idEntreprise")
    @ToString.Exclude
    private Entreprise entreprise;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Utilisateur that = (Utilisateur) o;
        return idUtilisateur != null && Objects.equals(idUtilisateur, that.idUtilisateur);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
