package com.romand.gestiondestockV1.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "entreprise")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idEntreprise")
public class Entreprise extends AbstractEntity{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idEntreprise;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "description")
    private String description;

    @Embedded
    private Adresse adresse;

    @Column(name = "code_fiscal", nullable = false, unique = true)
    private String codeFiscal;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "numero_telephone", nullable = false)
    private String numeroTelephone;

    @Column(name = "photo")
    private String photo;

    @OneToMany(mappedBy = "entreprise")
    @ToString.Exclude
    private List<Article> articles;

    @OneToMany(mappedBy = "entreprise")
    @ToString.Exclude
    private List<Utilisateur> utilisateurs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Entreprise that = (Entreprise) o;
        return idEntreprise != null && Objects.equals(idEntreprise, that.idEntreprise);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
