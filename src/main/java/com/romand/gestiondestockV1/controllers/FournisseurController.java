package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.FournisseurApi;
import com.romand.gestiondestockV1.dtos.FournisseurDto;
import com.romand.gestiondestockV1.services.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class FournisseurController implements FournisseurApi {
    private final FournisseurService fournisseurService;
    @Autowired
    public FournisseurController(FournisseurService fournisseurService) {
        this.fournisseurService = fournisseurService;
    }

    @Override
    public ResponseEntity<Page<FournisseurDto>> getAllFournisseurs(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.getAllFournisseurs(page, size));
    }
    @Override
    public ResponseEntity<FournisseurDto> getFournisseurById(String idFournisseur) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.getFournisseurById(idFournisseur));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<FournisseurDto> createFournisseur(FournisseurDto fournisseurDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(fournisseurService.createFournisseur(fournisseurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<FournisseurDto> updateFournisseur(String idFournisseur, FournisseurDto fournisseurDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(fournisseurService.updateFournisseur(idFournisseur, fournisseurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteFournisseurById(String idFournisseur) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.deleteFournisseurById(idFournisseur));
    }
}
