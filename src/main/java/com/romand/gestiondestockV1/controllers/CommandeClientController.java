package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.CommandeClientApi;
import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import com.romand.gestiondestockV1.services.CommandeClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommandeClientController implements CommandeClientApi {
    private final CommandeClientService commandeClientService;
    @Autowired
    public CommandeClientController(CommandeClientService commandeClientService) {
        this.commandeClientService = commandeClientService;
    }

    @Override
    public ResponseEntity<Page<CommandeClientDto>> getAllCommandesClient(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.getAllCommandesClient(page, size));
    }
    @Override
    public ResponseEntity<CommandeClientDto> getCommandeClientById(String idCommandeClient) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.getCommandeClientById(idCommandeClient));
    }
    @Override
    public ResponseEntity<Page<CommandeClientDto>> getAllCommandesClientByIdClient(String idClient, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(commandeClientService.getAllCommandesClientByIdClient(idClient, page,size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CommandeClientDto> createCommandeClient(CommandeClientDto commandeClientDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(commandeClientService.createCommandeClient(commandeClientDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CommandeClientDto> updateCommandeClient(String idCommandeClient, CommandeClientDto commandeClientDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(commandeClientService.updateCommandeClient(idCommandeClient, commandeClientDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteCommandeClientById(String idCommandeClient) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeClientService.deleteCommandeClient(idCommandeClient));
    }
}
