package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.RoleApi;
import com.romand.gestiondestockV1.dtos.RoleDto;
import com.romand.gestiondestockV1.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoleController implements RoleApi {
    private final RoleService roleService;
    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<Page<RoleDto>> getAllRoles(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.getAllRoles(page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<RoleDto> getRoleById(String idRole) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.getRoleById(idRole));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<RoleDto> createRole(RoleDto roleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.createRole(roleDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<RoleDto> updateRole(String idRole, RoleDto roleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.updateRole(idRole, roleDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteRoleById(String idRole) {
        return ResponseEntity.status(HttpStatus.OK).body(roleService.deleteRoleById(idRole));
    }
}
