package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.UtilisateurApi;
import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import com.romand.gestiondestockV1.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class UtilisateurController implements UtilisateurApi {
    private final UtilisateurService utilisateurService;
    @Autowired
    public UtilisateurController(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @Override
    public ResponseEntity<Page<UtilisateurDto>> getAllUtilisateurs(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(utilisateurService.getAllUtilisateurs(page, size));
    }
    @Override
    public ResponseEntity<UtilisateurDto> getUtilisateurById(String idUtilisateur) {
        return ResponseEntity.status(HttpStatus.OK).body(utilisateurService.getUtilisateurById(idUtilisateur));
    }
    @Override
    public ResponseEntity<Page<UtilisateurDto>> getAllUtilisateursByIdEntreprise(String idEntreprise, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(utilisateurService.getAllUtilisateursByIdEntreprise(
                idEntreprise, page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<UtilisateurDto> createUtilisateur(UtilisateurDto utilisateurDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(utilisateurService.createUtilisateur(utilisateurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<UtilisateurDto> updateUtilisateur(String idUtilisateur, UtilisateurDto utilisateurDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(utilisateurService.updateUtilisateur(idUtilisateur, utilisateurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteUTilisateurById(String idUtilisateur) {
        return ResponseEntity.status(HttpStatus.OK).body(utilisateurService.deleteUtilisateurById(idUtilisateur));
    }
}
