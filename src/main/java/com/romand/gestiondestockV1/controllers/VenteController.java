package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.VenteApi;
import com.romand.gestiondestockV1.dtos.VenteDto;
import com.romand.gestiondestockV1.services.VenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class VenteController implements VenteApi {
    private final VenteService venteService;
    @Autowired
    public VenteController(VenteService venteService) {
        this.venteService = venteService;
    }

    @Override
    public ResponseEntity<Page<VenteDto>> getAllVentes(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(venteService.getAllVentes(page, size));
    }
    @Override
    public ResponseEntity<VenteDto> getVenteById(String idVente) {
        return ResponseEntity.status(HttpStatus.OK).body(venteService.getVenteById(idVente));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<VenteDto> createVente(VenteDto venteDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(venteService.createVente(venteDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<VenteDto> updateVente(String idVente, VenteDto venteDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(venteService.updateVente(idVente, venteDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteVenteById(String idVente) {
        return ResponseEntity.status(HttpStatus.OK).body(venteService.deleteVenteById(idVente));
    }
}
