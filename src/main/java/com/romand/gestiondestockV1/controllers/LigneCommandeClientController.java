package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.LigneCommandeClientApi;
import com.romand.gestiondestockV1.dtos.LigneCommandeClientDto;
import com.romand.gestiondestockV1.services.LigneCommandeClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class LigneCommandeClientController implements LigneCommandeClientApi {
    private final LigneCommandeClientService ligneCommandeClientService;
    @Autowired
    public LigneCommandeClientController(LigneCommandeClientService ligneCommandeClientService) {
        this.ligneCommandeClientService = ligneCommandeClientService;
    }

    @Override
    public ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClient(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneCommandeClientService.getAllLignesCommandesClient(page, size));
    }
    @Override
    public ResponseEntity<LigneCommandeClientDto> getLigneCommandeClientById(String idLigneCommandeClient) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeClientService.getLigneCommandeClientById(idLigneCommandeClient));
    }
    @Override
    public ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClientByIdArticle(
            String idArticle, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeClientService.getAllLignesCommandeClientByIdArticle(idArticle, page, size));
    }
    @Override
    public ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClientByIdCommandeClient(
            String idCommandeClient, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeClientService.getAllLignesCommandeClientByIdCommandeClient(idCommandeClient, page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneCommandeClientDto> createLigneCommandeClient(LigneCommandeClientDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ligneCommandeClientService.createLigneCommandeClient(dto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneCommandeClientDto> updateLigneCommandeClient(String idLigneCommandeClient, LigneCommandeClientDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ligneCommandeClientService.updateLigneCommandeClient(idLigneCommandeClient, dto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteLigneCommandeClientById(String idLigneCommandeClient) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeClientService.deleteLigneCommandeClientById(idLigneCommandeClient));
    }
}
