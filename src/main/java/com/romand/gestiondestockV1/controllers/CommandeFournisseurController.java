package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.CommandeFournisseurApi;
import com.romand.gestiondestockV1.dtos.CommandeFournisseurDto;
import com.romand.gestiondestockV1.services.CommandeFournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommandeFournisseurController implements CommandeFournisseurApi {
    private final CommandeFournisseurService commandeFournisseurService;
    @Autowired
    public CommandeFournisseurController(CommandeFournisseurService commandeFournisseurService) {
        this.commandeFournisseurService = commandeFournisseurService;
    }

    @Override
    public ResponseEntity<Page<CommandeFournisseurDto>> getAllCommandesFournisseur(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(commandeFournisseurService.getAllCommandesFournisseur(page, size));
    }
    @Override
    public ResponseEntity<CommandeFournisseurDto> getCommandeFournisseurById(String idCommandeFournisseur) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(commandeFournisseurService.getCommandeFournisseurById(idCommandeFournisseur));
    }
    @Override
    public ResponseEntity<Page<CommandeFournisseurDto>> getAllCommandesFournisseurByIdFournisseur(
            String idFournisseur, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(commandeFournisseurService.getAllCommandesFournisseurByIdFournisseur(idFournisseur, page,size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CommandeFournisseurDto> createCommandeFournisseur(CommandeFournisseurDto commandeFournisseurDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(commandeFournisseurService.createCommandeFournisseur(commandeFournisseurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CommandeFournisseurDto> updateCommandeFournisseur(
            String idCommandeFournisseur, CommandeFournisseurDto commandeFournisseurDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(commandeFournisseurService.updateCommandeFournisseur(idCommandeFournisseur, commandeFournisseurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteCommandeFournisseurById(String idCommandeFournisseur) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(commandeFournisseurService.deleteCommandeFournisseurById(idCommandeFournisseur));
    }
}
