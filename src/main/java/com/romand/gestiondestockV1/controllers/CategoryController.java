package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.CategoryApi;
import com.romand.gestiondestockV1.dtos.CategoryDto;
import com.romand.gestiondestockV1.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController implements CategoryApi {
    private final CategoryService categoryService;
    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public ResponseEntity<Page<CategoryDto>> getAllCategories(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.getAllCategories(page, size));
    }
    @Override
    public ResponseEntity<CategoryDto> getCategoryById(String idCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.getCategoryById(idCategory));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CategoryDto> createCategory(CategoryDto categoryDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.createCategory(categoryDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<CategoryDto> updateCategory(String idCategory, CategoryDto categoryDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.updateCategory(idCategory, categoryDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteCategoryById(String idCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.deleteCategoryById(idCategory));
    }
}
