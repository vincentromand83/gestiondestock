package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.ArticleDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/articles")
public interface ArticleApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les articles ou une liste vide " +
            "si aucun article n'est enregistré", summary = "LISTER TOUS LES ARTICLES")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des articles ou " +
                    "une liste vide si aucun article n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<ArticleDto>> getAllArticles(@RequestParam(name = "page", defaultValue = "0") int page,
                                                           @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un article par son ID",
            summary = "RECHERCHER UN ARTICLE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi l'article avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucun article trouvé avec l'ID fourni")
    })
    ResponseEntity<ArticleDto> getArticleById(@PathVariable("idArticle") String idArticle);

    @GetMapping(value = "/category/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de tous les articles " +
            "ayant la catégorie fourni", summary = "LISTER TOUS LES ARTICLES AVEC CETTE CATEGORIE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de tous les articles " +
                    "avec la catégorie fourni ou une liste vide si aucun article est enregistré avec cette catégorie"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<ArticleDto>> getAllArticlesByIdCategory(
            @PathVariable("idCategory") String idCategory,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/entreprise/{idEntreprise}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de tous les articles " +
            "appartenant à l'entreprise fourni", summary = "LISTER TOUS LES ARTICLES DE CETTE ENTREPRISE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de tous les articles " +
                    "appartenant à l'entreprise fourni"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<ArticleDto>> getAllArticlesByIdEntreprise(
            @PathVariable("idEntreprise") String idEntreprise,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouvel article",
            summary = "ENREGISTRER UN ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'article a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "L'article fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<ArticleDto> createArticle(@Valid @RequestBody ArticleDto articleDto);

    @PutMapping(value = "/update/{idArticle}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un article avec son ID",
            summary = "MODIFIER UN ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'article a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "L'article fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun article avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<ArticleDto> updateArticle(@Valid @PathVariable("idArticle") String idArticle,
                                                    @RequestBody ArticleDto articleDto);

    @DeleteMapping("/delete/{idArticle}")
    @Operation(description = "Cette methode permet de supprimer un article avec son ID",
            summary = "SUPPRIMER UN ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'article avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun article avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteArticleById(@PathVariable("idArticle") String idArticle);
}
