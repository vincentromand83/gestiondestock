package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.MouvementStockDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/mouvementstocks")
public interface MouvementStockApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les mouvements de ou une liste vide " +
            "si aucun mouvement de stock n'est enregistré", summary = "LISTER TOUS LES MOUVEMENTS DE STOCK")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des mouvements de stock ou " +
                    "une liste vide si aucun mouvement de stock n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<MouvementStockDto>> getAllMouvementsStock(@RequestParam(name = "page", defaultValue = "0") int page,
                                                                  @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idMouvementStock}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un mouvement de stock par son ID",
            summary = "RECHERCHER UN MOUVEMENT DE STOCK PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi le mouvement de stock avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucun mouvement de stock trouvé avec l'ID fourni")
    })
    ResponseEntity<MouvementStockDto> getMouvementStockById(@PathVariable("idMouvementStock") String idMouvementStock);

    @GetMapping(value = "/article/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de tous les mouvements de stock" +
            "ayant l'article fourni", summary = "LISTER TOUS LES MOUVEMENTS DE STOCK AVEC CET ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de tous les mouvements stock " +
                    "avec l'article fournit ou une liste vide si aucun mouvement de stock est enregistré avec cet article"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<MouvementStockDto>> getAllMouvementsStockByIdArticle(
            @PathVariable("idArticle") String idArticle,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouveau mouvement de stock",
            summary = "ENREGISTRER UN MOUVEMENT DE STOCK")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le mouvement de stock a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "Le mouvement de stock fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<MouvementStockDto> createMouvementStock(@Valid @RequestBody MouvementStockDto mouvementStockDto);

    @PutMapping(value = "/update/{idMouvementStock}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un mouvement de stock avec son ID",
            summary = "MODIFIER UN MOUVEMENT DE STOCK")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le mouvement de stock a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "Le mouvement de stock fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun mouvement de stock avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<MouvementStockDto> updateMouvementStock(@PathVariable("idMouvementStock") String idMouvementStock,
                                                           @Valid @RequestBody MouvementStockDto mouvementStockDto);

    @DeleteMapping("/delete/{idMouvementStock}")
    @Operation(description = "Cette methode permet de supprimer un mouvement de stock avec son ID",
            summary = "SUPPRIMER UN MOUVEMENT DE STOCK")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Le mouvement de stock avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun mouvement de stock avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteMouvementStockById(@PathVariable("idMouvementStock") String idMouvementStock);
}
