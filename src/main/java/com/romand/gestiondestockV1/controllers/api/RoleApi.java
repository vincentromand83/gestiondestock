package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.RoleDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/roles")
public interface RoleApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les roles ou une liste vide " +
            "si aucun role n'est enregistré", summary = "LISTER TOUS LES ROLES")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des roles ou " +
                    "une liste vide si aucun role n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<Page<RoleDto>> getAllRoles(@RequestParam(name = "page", defaultValue = "0") int page,
                                                     @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idRole}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un role par son ID",
            summary = "RECHERCHER UN ROLE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi le role avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun role trouvé avec l'ID fourni")
    })
    ResponseEntity<RoleDto> getRoleById(@PathVariable("idRole") String idRole);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouveau role",
            summary = "ENREGISTRER UN ROLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le role a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "Le role fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<RoleDto> createRole(@Valid @RequestBody RoleDto roleDto);

    @PutMapping(value = "/update/{idRole}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un role avec son ID",
            summary = "MODIFIER UN ROLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le role a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "Le role fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun role avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<RoleDto> updateRole(@PathVariable("idRole") String idRole, @Valid @RequestBody RoleDto roleDto);

    @DeleteMapping(value = "/delete/{idRole}")
    @Operation(description = "Cette methode permet de supprimer un role avec son ID",
            summary = "SUPPRIMER UN ROLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Le role avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun role avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteRoleById(@PathVariable("idRole") String idRole);
}
