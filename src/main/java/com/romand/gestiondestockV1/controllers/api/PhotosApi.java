package com.romand.gestiondestockV1.controllers.api;

import com.flickr4java.flickr.FlickrException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

@RequestMapping("gestiondestock/v1/photos")
public interface PhotosApi {

    @PostMapping("/create/{id}/{title}/{context}")
    ResponseEntity<Object> savePhoto(@PathVariable("context") String context, @PathVariable("id") String id,
                                    @RequestPart("file") MultipartFile photo, @PathVariable("title") String title)
            throws IOException, FlickrException;
}
