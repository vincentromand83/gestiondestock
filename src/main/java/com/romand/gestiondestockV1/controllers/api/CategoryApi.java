package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.CategoryDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/categories")
public interface CategoryApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les categoriesou une liste vide " +
            "si aucune categorie n'est enregistrée", summary = "LISTER TOUTES LES CATEGORIES")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des categories ou " +
                    "une liste vide si aucune categorie n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<CategoryDto>> getAllCategories(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une categorie par son ID",
            summary = "RECHERCHER UNE CATEGORIE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la categorie avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune categorie trouvée avec l'ID fourni")
    })
    ResponseEntity<CategoryDto> getCategoryById(@PathVariable("idCategory") String idCategory);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle categorie",
            summary = "ENREGISTRER UNE CATEGORIE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La categorie a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La categorie fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CategoryDto categoryDto);

    @PutMapping(value = "/update/{idCategory}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une catgeorie avec son ID",
            summary = "MODIFIER UNE CATEGORIE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La categorie a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La categorie fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune categorie avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<CategoryDto> updateCategory(@PathVariable("idCategory") String idCategory,
                                               @RequestBody CategoryDto categoryDto);

    @DeleteMapping(value = "/delete/{idCategory}")
    @Operation(description = "Cette methode permet de supprimer une catégorie avec son ID",
            summary = "SUPPRIMER UNE CATEGORIE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La catégorie avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune catégorie avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteCategoryById(@PathVariable("idCategory") String idCategory);
}
