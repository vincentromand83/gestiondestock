package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/commandeclients")
public interface CommandeClientApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les commandes client ou une liste vide " +
            "si aucune commande client n'est enregistrée", summary = "LISTER TOUTES LES COMMANDES CLIENTS")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des commandes client ou " +
                    "une liste vide si aucune commande client n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<CommandeClientDto>> getAllCommandesClient(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size",defaultValue = "10") int size);

    @GetMapping(value = "/{idCommandeClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une commande client par son ID",
            summary = "RECHERCHER UNE COMMANDE CLIENT PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la commande client avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune commande client trouvée avec l'ID fourni")
    })
    ResponseEntity<CommandeClientDto> getCommandeClientById(@PathVariable("idCommandeClient") String idCommandeClient);

    @GetMapping(value = "/client/{idClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes les commandes clients " +
            "ayant le client fourni", summary = "LISTER TOUTES LES COMMANDES CLIENTS AVEC CE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les commandes clients " +
                    "avec le client fourni ou une liste vide si aucune commande client est enregistré avec ce client"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<CommandeClientDto>> getAllCommandesClientByIdClient(
            @PathVariable("idClient") String idClient,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle commande client",
            summary = "ENREGISTRER UNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La commande client a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La commande client fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<CommandeClientDto> createCommandeClient(@Valid @RequestBody CommandeClientDto commandeClientDto);

    @PutMapping(value = "/update/{idCommandeClient}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une commande client avec son ID",
            summary = "MODIFIER UNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La commande client a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La commande fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune commande client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<CommandeClientDto> updateCommandeClient(@PathVariable("idCommandeClient") String idCommandeClient,
                                                           @Valid @RequestBody CommandeClientDto commandeClientDto);

    @DeleteMapping(value = "/delete/{idCommandeClient}")
    @Operation(description = "Cette methode permet de supprimer une commande client avec son ID",
            summary = "SUPPRIMER UNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La commande client avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune commande client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteCommandeClientById(@PathVariable("idCommandeClient") String idCommandeClient);
}
