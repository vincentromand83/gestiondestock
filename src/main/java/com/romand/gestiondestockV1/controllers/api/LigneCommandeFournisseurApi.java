package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.LigneCommandeFournisseurDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/lignecommandefournisseurs")
public interface LigneCommandeFournisseurApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les lignes commande fournisseur ou " +
            "une liste vide si aucune ligne commande fournisseur n'est enregistrée",
            summary = "LISTER TOUTES LES LIGNES COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des lignes commande fournisseur ou " +
                    "une liste vide si aucune ligne commande fournisseur n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseur(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idLigneCommandeFournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une ligne commande fournisseur par son ID",
            summary = "RECHERCHER UNE LIGNE COMMANDE FOURNISSEUR PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la ligne commande fournisseur avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande fournisseur trouvée avec l'ID fourni")
    })
    ResponseEntity<LigneCommandeFournisseurDto> getLigneCommandeFournisseurById(
            @PathVariable("idLigneCommandeFournisseur") String idLigneCommandeFournisseur);

    @GetMapping(value = "/article/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes ligne commande fournisseur " +
            "ayant l'article fourni", summary = "LISTER TOUTES LES LIGNES COMMANDES FOURNISSEUR AVEC CET ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes commande fournisseur " +
                    "avec l'article fourni ou une liste vide si aucune ligne commande fournisseur est enregistré " +
                    "avec cet article"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseurByIdArticle(
            @PathVariable("idArticle") String idArticle,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/commandefournisseur/{idCommandeFournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes les lignes commande fournisseur " +
            "ayant la commande fournisseur fourni",
            summary = "LISTER TOUTES LES LIGNES COMMANDE FOURNISSEUR AVEC CETTE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes commandes fournisseur " +
                    "avec la commande fournisseur fourni ou une liste vide si aucune ligne commande fournisseur " +
                    "est enregistré avec cette commande fournisseur"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseurByIdCommandeFournisseur(
            @PathVariable("idCommandeFournisseur") String idCommandeFopurnisseur,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle ligne commande fournisseur",
            summary = "ENREGISTRER UNE LIGNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne commande fournisseur a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne commande fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<LigneCommandeFournisseurDto> createLigneCommandeFournisseur(
            @Valid @RequestBody LigneCommandeFournisseurDto ligneCommandeFournisseurDto);

    @PutMapping(value = "/update/{idLigneCommandeFournisseur}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une ligne commande fournisseur avec son ID",
            summary = "MODIFIER UNE LIGNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne commande fournisseur a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne commande fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<LigneCommandeFournisseurDto> updateLigneCommandeFournisseur(
            @PathVariable("idLigneCommandeFournisseur") String idLigneCommandeFournisseur,
            @Valid @RequestBody LigneCommandeFournisseurDto dto);

    @DeleteMapping(value = "/delete.{idLigneCommandeFournisseur}")
    @Operation(description = "Cette methode permet de supprimer une ligne commande fournisseur avec son ID",
            summary = "SUPPRIMER UNE LIGNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La ligne commande fournisseur avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteLigneCommandeFournisseurById(
            @PathVariable("idLigneCommandeFournisseur") String idLigneCommandeFournisseur);
}
