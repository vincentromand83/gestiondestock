package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.CommandeFournisseurDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/commandefournisseurs")
public interface CommandeFournisseurApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les commandes fournisseur ou une liste vide " +
            "si aucune commande fournisseur n'est enregistrée", summary = "LISTER TOUTES LES COMMANDES FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des commandes fournisseur ou " +
                    "une liste vide si aucune commande fournisseur n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<CommandeFournisseurDto>> getAllCommandesFournisseur(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idCommandeFournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une commande fournisseur par son ID",
            summary = "RECHERCHER UNE COMMANDE FOURNISSEUR PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la commande fournisseur avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune commande fournisseur trouvée avec l'ID fourni")
    })
    ResponseEntity<CommandeFournisseurDto> getCommandeFournisseurById(
            @PathVariable("idCommandeFournisseur") String idCommandeFournisseur);

    @GetMapping(value = "/fournisseur/{idFournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes les commandes fournisseur " +
            "ayant le fournisseur fourni", summary = "LISTER TOUTES LES COMMANDE FOURNISSEUR AVEC CE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les commandes fournisseur " +
                    "avec le fournisseur fourni ou une liste vide si aucune commande fournisseur est enregistré avec ce fournisseur"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<CommandeFournisseurDto>> getAllCommandesFournisseurByIdFournisseur(
            @PathVariable("idFournisseur") String idFournisseur,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle commande fournisseur",
            summary = "ENREGISTRER UNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La commande fournisseur a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La commande fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<CommandeFournisseurDto> createCommandeFournisseur(
            @Valid @RequestBody CommandeFournisseurDto commandeFournisseurDto);

    @PutMapping(value = "/update/{idCommandeFournisseur}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une commande fournisseur avec son ID",
            summary = "MODIFIER UNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La commande fournisseur a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La commande fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune commande fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<CommandeFournisseurDto> updateCommandeFournisseur(
            @PathVariable("idCommandeFournisseur") String idCommandeFournisseur,
            @Valid @RequestBody CommandeFournisseurDto commandeFournisseurDto);

    @DeleteMapping(value = "/delete/{idCommandeFournisseur}")
    @Operation(description = "Cette methode permet de supprimer une commande fournisseur avec son ID",
            summary = "SUPPRIMER UNE COMMANDE FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La commande fournisseur avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune commande fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteCommandeFournisseurById(
            @PathVariable("idCommandeFournisseur") String idCommandeFournisseur);
}
