package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/utilisateurs")
public interface UtilisateurApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les utilisateurs ou une liste vide " +
            "si aucun utilisateur n'est enregistré", summary = "LISTER TOUS LES UTILISATEURS")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des utilisateurs ou " +
                    "une liste vide si aucun utilisateur n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<UtilisateurDto>> getAllUtilisateurs(@RequestParam(name = "page", defaultValue = "0") int page,
                                                            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idUtilisateur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un utilisateur par son ID",
            summary = "RECHERCHER UN UTILISATEUR PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi l'utilisateur avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucun utilisateur trouvé avec l'ID fourni")
    })
    ResponseEntity<UtilisateurDto> getUtilisateurById(@PathVariable("idUtilisateur") String idUtilisateur);

    @GetMapping(value = "/entreprise/{idEntreprise}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<UtilisateurDto>> getAllUtilisateursByIdEntreprise(
            @PathVariable("idEntreprise") String idEntreprise,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouvel utilisateur",
            summary = "ENREGISTRER UN UTILISATEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'utilisateur a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "L'utilisateur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<UtilisateurDto> createUtilisateur(@Valid @RequestBody UtilisateurDto utilisateurDto);

    @PutMapping(value = "/update/{idUtilisateur}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un utilisateur avec son ID",
            summary = "MODIFIER UN UTILISATEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'utilisateur a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "L'utilisateur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun utilisateur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<UtilisateurDto> updateUtilisateur(@PathVariable("idUtilisateur") String idUtilisateur,
                                                     @Valid @RequestBody UtilisateurDto utilisateurDto);

    @DeleteMapping(value = "/delete/{idUtilisateur}")
    @Operation(description = "Cette methode permet de supprimer un utilisateur avec son ID",
            summary = "SUPPRIMER UN UTILISATEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'utilisateur avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun utilisateur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteUTilisateurById(@PathVariable("idUtilisateur") String idUtilisateur);
}
