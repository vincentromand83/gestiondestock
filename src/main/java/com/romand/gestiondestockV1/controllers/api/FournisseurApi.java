package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.FournisseurDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/fournisseurs")
public interface FournisseurApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les fournisseurs ou une liste vide " +
            "si aucun fournisseur n'est enregistré", summary = "LISTER TOUS LES FOURNISSEURS")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des fournisseurs ou " +
                    "une liste vide si aucun fournisseur n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<FournisseurDto>> getAllFournisseurs(@RequestParam(name = "page", defaultValue = "0") int page,
                                                            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idFournisseur}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un fournisseur par son ID",
            summary = "RECHERCHER UN FOURNISSEUR PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi le fournisseur avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucun fournisseur trouvé avec l'ID fourni")
    })
    ResponseEntity<FournisseurDto> getFournisseurById(@PathVariable("idFournisseur") String idFournisseur);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouveau fournisseur",
            summary = "ENREGISTRER UN FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le fournisseur a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "Le fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<FournisseurDto> createFournisseur(@Valid @RequestBody FournisseurDto fournisseurDto);

    @PutMapping(value = "/update/{idFournisseur}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un fournisseur avec son ID",
            summary = "MODIFIER UN FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le fournisseur a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "Le fournisseur fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<FournisseurDto> updateFournisseur(@Valid @PathVariable("idFournisseur") String idFournisseur,
                                                     @RequestBody FournisseurDto fournisseurDto);

    @DeleteMapping(value = "/delete/{idFournisseur}")
    @Operation(description = "Cette methode permet de supprimer un fournisseur avec son ID",
            summary = "SUPPRIMER UN FOURNISSEUR")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Le fournisseur avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun fournisseur avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteFournisseurById(@PathVariable("idFournisseur") String idFournisseur);
}
