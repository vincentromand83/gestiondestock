package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/entreprises")
public interface EntrepriseApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les entreprises ou une liste vide " +
            "si aucune entreprise n'est enregistrée", summary = "LISTER TOUTES LES ENTREPRISES")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des entreprises ou " +
                    "une liste vide si aucune entreprise n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<EntrepriseDto>> getAllEntreprises(@RequestParam(name = "page", defaultValue = "0") int page,
                                                          @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idEntreprise}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une entreprise par son ID",
            summary = "RECHERCHER UNE ENTREPRISE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi l'entreprise avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune entreprise trouvée avec l'ID fourni")
    })
    ResponseEntity<EntrepriseDto> getEntrepriseById(@PathVariable("idEntreprise") String idEntreprise);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle entreprise",
            summary = "ENREGISTRER UNE ENTREPRISE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'entreprise a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "L'entreprise fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<EntrepriseDto> createEntreprise(@Valid @RequestBody EntrepriseDto entrepriseDto);

    @PutMapping(value = "/update/{idEntreprise}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une entreprise avec son ID",
            summary = "MODIFIER UNE ENTREPRISE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "L'entreprise a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "L'entreprise fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune entreprise avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<EntrepriseDto> updateEntreprise(@PathVariable("idEntreprise") String idEntreprise,
                                                   @Valid @RequestBody EntrepriseDto entrepriseDto);

    @DeleteMapping("/delete/{idEntreprise}")
    @Operation(description = "Cette methode permet de supprimer une entreprise avec son ID",
            summary = "SUPPRIMER UNE ENTREPRISE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'entreprise avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune entreprise avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteEntrepriseById(@PathVariable("idEntreprise") String idEntreprise);
}
