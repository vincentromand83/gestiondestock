package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.VenteDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/ventes")
public interface VenteApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les ventes ou une liste vide " +
            "si aucune vente n'est enregistrée", summary = "LISTER TOUTES LES VENTES")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des ventes ou " +
                    "une liste vide si aucune vente n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<VenteDto>> getAllVentes(@RequestParam(name = "page", defaultValue = "0") int page,
                                                @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idVente}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une vente par son ID",
            summary = "RECHERCHER UNE VENTE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la vente avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune vente trouvée avec l'ID fourni")
    })
    ResponseEntity<VenteDto> getVenteById(@PathVariable("idVente") String idVente);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle vente",
            summary = "ENREGISTRER UNE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La vente a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La vente fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<VenteDto> createVente(@Valid @RequestParam VenteDto venteDto);

    @PutMapping(value = "/update/{idVente}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une vente avec son ID",
            summary = "MODIFIER UNE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La vente a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La vente fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune vente avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<VenteDto> updateVente(@PathVariable("idVente") String idVente,
                                         @Valid @RequestBody VenteDto venteDto);

    @DeleteMapping(value = "/delete/{idVente}")
    @Operation(description = "Cette methode permet de supprimer une vente avec son ID",
            summary = "SUPPRIMER UNE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La vente avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune vente avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteVenteById(@PathVariable("idVente") String idVente);
}
