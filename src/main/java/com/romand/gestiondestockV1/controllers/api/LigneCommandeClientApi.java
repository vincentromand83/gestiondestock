package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.LigneCommandeClientDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/lignecommandeclients")
public interface LigneCommandeClientApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les lignes commande client ou une " +
            "liste vide si aucune ligne commande client n'est enregistrée",
            summary = "LISTER TOUTES LES LIGNES COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des lignes commande client ou " +
                    "une liste vide si aucune ligne commande client n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClient(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idLigneCommandeClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une ligne commande client par son ID",
            summary = "RECHERCHER UNE LIGNE COMMANDE CLIENT PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la ligne commande client avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande client trouvée avec l'ID fourni")
    })
    ResponseEntity<LigneCommandeClientDto> getLigneCommandeClientById(
            @PathVariable("idLigneCommandeClient") String idLigneCommandeClient);

    @GetMapping(value = "/article/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes les lignes commande client " +
            "ayant l'article fourni", summary = "LISTER TOUTES LES LIGNES COMMANDE CLIENT AVEC CET ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes commande client " +
                    "avec l'article fourni ou une liste vide si aucune ligne commande client est enregistré avec cet article"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClientByIdArticle(
            @PathVariable("idArticle") String idArticle,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/commandeclient/{idCommandeClient}")
    @Operation(description = "Cette methode permet de retourner la liste de toutes les commandes clients " +
            "ayant la commande client fourni", summary = "LISTER TOUTES LES LIGNES COMMANDE CLIENT AVEC CETTE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes commande client " +
                    "avec la commande client fourni ou une liste vide si aucune ligne commande client est enregistré " +
                    "cette commande client"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneCommandeClientDto>> getAllLignesCommandeClientByIdCommandeClient(
            @PathVariable(name = "idCommandeClient") String idCommandeClient,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle ligne commande client",
            summary = "ENREGISTRER UNE LIGNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne commande client a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne commande client fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<LigneCommandeClientDto> createLigneCommandeClient(@Valid @RequestBody LigneCommandeClientDto dto);

    @PutMapping(value = "/update/{idLigneCommandeClient}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une ligne commande client avec son ID",
            summary = "MODIFIER UNE LIGNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne commande client a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne commande client fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<LigneCommandeClientDto> updateLigneCommandeClient(
            @PathVariable("idLigneCommandeClient") String idLigneCommandeClient,
            @Valid @RequestBody LigneCommandeClientDto dto);

    @DeleteMapping("/delete/{idLigneCommandeClient}")
    @Operation(description = "Cette methode permet de supprimer une ligne commande client avec son ID",
            summary = "SUPPRIMER UNE LIGNE COMMANDE CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La ligne commande client avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne commande client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteLigneCommandeClientById(
            @PathVariable("idLigneCommandeClient")String idLigneCommandeClient);
}
