package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.LigneVenteDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/ligneventes")
public interface LigneVenteApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de toutes les lignes vente ou une liste vide " +
            "si aucune ligne vente n'est enregistrée", summary = "LISTER TOUTES LES LIGNES VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des lignes vente ou " +
                    "une liste vide si aucune ligne vente n'est enregistrée"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneVenteDto>> getAllLignesVente(@RequestParam(name = "page", defaultValue = "0")int page,
                                                          @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/{idLigneVente}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher une ligne de vente par son ID",
            summary = "RECHERCHER UNE LIGNE DE VENTE PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la ligne de vente avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne de vente trouvée avec l'ID fourni")
    })
    ResponseEntity<LigneVenteDto> getLigneVenteById(@PathVariable("idLigneVente") String idLigneVente);

    @GetMapping(value = "/article/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de toutes les lignes vente " +
            "ayant l'article fourni", summary = "LISTER TOUTES LES LIGNE VENTE AVEC CET ARTICLE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes vente " +
                    "avec l'article fourni ou une liste vide si aucune ligne vente est enregistré avec cet article"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneVenteDto>> getAllLignesVenteByIdArticle(@PathVariable("idArticle") String idArticle,
                                                                     @RequestParam(name = "page", defaultValue = "0") int page,
                                                                     @RequestParam(name = "size", defaultValue = "10") int size);

    @GetMapping(value = "/vente/{idVente}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de retourner la liste de tous les ligne vente " +
            "ayant la vente fourni", summary = "LISTER TOUTES LES LIGNES VENTE AVEC CETTE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste de toutes les lignes vente " +
                    "avec la vente fourni ou une liste vide si aucune ligne vente est enregistré avec cette vente"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<LigneVenteDto>> getAllLignesVenteByIdVente(@PathVariable("idVente") String idVente,
                                                                   @RequestParam(name = "page", defaultValue = "0") int page,
                                                                   @RequestParam(name = "size", defaultValue = "10") int size);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer une nouvelle ligne de vente",
            summary = "ENREGISTRER UNE LIGNE DE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne de vente a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne de vente n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<LigneVenteDto> createLigneVente(@Valid @RequestBody LigneVenteDto ligneVenteDto);

    @PutMapping(value = "/update/{idLigneVente}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier une ligne de vente avec son ID",
            summary = "MODIFIER UNE LIGNE DE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "La ligne de vente a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "La ligne de vente fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne de vente avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<LigneVenteDto> updateLigneVente(@PathVariable("idLigneVente") String idLigneVente,
                                                   @Valid @RequestBody LigneVenteDto ligneVenteDto);

    @DeleteMapping(value = "/delete/{idLigneVente}")
    @Operation(description = "Cette methode permet de supprimer une ligne de vente avec son ID",
            summary = "SUPPRIMER UNE LIGNE DE VENTE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "La ligne de vente avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucune ligne de vente avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteLigneVenteById(@PathVariable("idLigneVente") String idLigneVente);
}
