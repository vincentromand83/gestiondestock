package com.romand.gestiondestockV1.controllers.api;

import com.romand.gestiondestockV1.dtos.ClientDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@RequestMapping("/gestiondestock/v1/clients")
public interface ClientApi {

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de renvoyer la liste de tous les clients ou une liste vide " +
            "si aucun client n'est enregistré", summary = "LISTER TOUS LES CLIENTS")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi la liste des clients ou " +
                    "une liste vide si aucun client n'est enregistré"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté")
    })
    ResponseEntity<Page<ClientDto>> getAllClients(@RequestParam(name = "page", defaultValue = "0")int page,
                                                  @RequestParam(name = "size", defaultValue = "10")int size);

    @GetMapping(value = "/{idClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de rechercher un client par son ID",
            summary = "RECHERCHER UN CLIENT PAR SON ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Renvoi le client avec l'ID recherché"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "404", description = "Aucun client trouvé avec l'ID fourni")
    })
    ResponseEntity<ClientDto> getClientById(@PathVariable("idClient") String idClient);

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet d'enregistrer un nouveau client",
            summary = "ENREGISTRER UN CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le client a été crée avec succés"),
            @ApiResponse(responseCode = "400", description = "Le client fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin")
    })
    ResponseEntity<ClientDto> createClient(@Valid @RequestBody ClientDto clientDto);

    @PutMapping(value = "/update/{idClient}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Cette methode permet de modifier un client avec son ID",
            summary = "MODIFIER UN CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Le client a été modifié avec succés"),
            @ApiResponse(responseCode = "400", description = "Le client fournit n'est pas valide"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'admin"),
            @ApiResponse(responseCode = "404", description = "Aucun client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<ClientDto> updateClient(@PathVariable("idClient") String idClient,
                                           @Valid @RequestBody ClientDto clientDto);

    @DeleteMapping(value = "/delete/{idClient}")
    @Operation(description = "Cette methode permet de supprimer un client avec son ID",
            summary = "SUPPRIMER UN CLIENT")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Le client avec l'ID fourni a été supprimé avec succés"),
            @ApiResponse(responseCode = "401", description = "L'utilisateur n'est pas connecté"),
            @ApiResponse(responseCode = "403", description = "L'utilisateur n'est pas connecté en tant qu'administrateur"),
            @ApiResponse(responseCode = "404", description = "Aucun client avec l'ID fourni n'a été trouvé")
    })
    ResponseEntity<String> deleteClientById(@PathVariable("idClient") String idClient);
}
