package com.romand.gestiondestockV1.controllers;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.controllers.api.PhotosApi;
import com.romand.gestiondestockV1.services.strategy.StrategyPhotoContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class PhotoController implements PhotosApi {

    private final StrategyPhotoContext strategyPhotoContext;
    @Autowired
    public PhotoController(StrategyPhotoContext strategyPhotoContext) {
        this.strategyPhotoContext = strategyPhotoContext;
    }

    @Override
    public ResponseEntity<Object> savePhoto(String context, String id, MultipartFile photo, String title)
            throws IOException, FlickrException {
        return ResponseEntity.status(HttpStatus.CREATED).body(strategyPhotoContext.savePhoto(
                context, id, photo.getInputStream(), title));
    }
}
