package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.EntrepriseApi;
import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import com.romand.gestiondestockV1.services.EntrepriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class EntrepriseController implements EntrepriseApi {
    private final EntrepriseService entrepriseService;
    @Autowired
    public EntrepriseController(EntrepriseService entrepriseService) {
        this.entrepriseService = entrepriseService;
    }

    @Override
    public ResponseEntity<Page<EntrepriseDto>> getAllEntreprises(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(entrepriseService.getAllEntreprises(page, size));
    }
    @Override
    public ResponseEntity<EntrepriseDto> getEntrepriseById(String idEntreprise) {
        return ResponseEntity.status(HttpStatus.OK).body(entrepriseService.getEntrepriseById(idEntreprise));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<EntrepriseDto> createEntreprise(EntrepriseDto entrepriseDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(entrepriseService.createEntreprise(entrepriseDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<EntrepriseDto> updateEntreprise(String idEntreprise, EntrepriseDto entrepriseDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(entrepriseService.updateEntreprise(idEntreprise, entrepriseDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteEntrepriseById(String idEntreprise) {
        return ResponseEntity.status(HttpStatus.OK).body(entrepriseService.deleteEntrepriseById(idEntreprise));
    }
}
