package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.ArticleApi;
import com.romand.gestiondestockV1.dtos.ArticleDto;
import com.romand.gestiondestockV1.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class ArticleController implements ArticleApi {
    private final ArticleService articleService;
    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Override
    public ResponseEntity<Page<ArticleDto>> getAllArticles(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.getAllArticles(page, size));
    }
    @Override
    public ResponseEntity<ArticleDto> getArticleById(String idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.getArticleById(idArticle));
    }
    @Override
    public ResponseEntity<Page<ArticleDto>> getAllArticlesByIdCategory(String idCategory, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.getAllArticlesByIdCategory(idCategory,page,size));
    }
    @Override
    public ResponseEntity<Page<ArticleDto>> getAllArticlesByIdEntreprise(String idEntreprise, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.getAllArticlesByIdEntreprise(idEntreprise, page,size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<ArticleDto> createArticle(ArticleDto articleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(articleService.createArticle(articleDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<ArticleDto> updateArticle(String idArticle, ArticleDto articleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(articleService.updateArticle(idArticle, articleDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteArticleById(String idArticle) {
        return ResponseEntity.status(HttpStatus.OK).body(articleService.deleteArticleById(idArticle));
    }
}
