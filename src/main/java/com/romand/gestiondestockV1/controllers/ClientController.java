package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.ClientApi;
import com.romand.gestiondestockV1.dtos.ClientDto;
import com.romand.gestiondestockV1.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientController implements ClientApi {
    private final ClientService clientService;
    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public ResponseEntity<Page<ClientDto>> getAllClients(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.getAllClients(page, size));
    }
    @Override
    public ResponseEntity<ClientDto> getClientById(String idClient) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.getClientById(idClient));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<ClientDto> createClient(ClientDto clientDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.createClient(clientDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<ClientDto> updateClient(String idClient, ClientDto clientDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.updateClient(idClient, clientDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteClientById(String idClient) {
        return ResponseEntity.status(HttpStatus.OK).body(clientService.deleteClientById(idClient));
    }
}
