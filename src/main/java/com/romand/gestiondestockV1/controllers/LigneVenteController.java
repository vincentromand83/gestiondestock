package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.LigneVenteApi;
import com.romand.gestiondestockV1.dtos.LigneVenteDto;
import com.romand.gestiondestockV1.services.LigneVenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class LigneVenteController implements LigneVenteApi {
    private final LigneVenteService ligneVenteService;
    @Autowired
    public LigneVenteController(LigneVenteService ligneVenteService) {
        this.ligneVenteService = ligneVenteService;
    }

    @Override
    public ResponseEntity<Page<LigneVenteDto>> getAllLignesVente(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneVenteService.getAllLignesVente(page, size));
    }
    @Override
    public ResponseEntity<LigneVenteDto> getLigneVenteById(String idLigneVente) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneVenteService.getLigneVenteById(idLigneVente));
    }
    @Override
    public ResponseEntity<Page<LigneVenteDto>> getAllLignesVenteByIdArticle(String idArticle, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneVenteService.getAllLignesVenteByIdArticle(idArticle, page, size));
    }
    @Override
    public ResponseEntity<Page<LigneVenteDto>> getAllLignesVenteByIdVente(String idVente, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneVenteService.getAllLignesVenteByIdVente(idVente, page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneVenteDto> createLigneVente(LigneVenteDto ligneVenteDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ligneVenteService.createLigneVente(ligneVenteDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneVenteDto> updateLigneVente(String idLigneVente, LigneVenteDto ligneVenteDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ligneVenteService.updateLigneVente(idLigneVente, ligneVenteDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteLigneVenteById(String idLigneVente) {
        return ResponseEntity.status(HttpStatus.OK).body(ligneVenteService.deleteLigneVenteById(idLigneVente));
    }
}
