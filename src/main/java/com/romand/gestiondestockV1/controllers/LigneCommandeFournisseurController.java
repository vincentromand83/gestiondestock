package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.LigneCommandeFournisseurApi;
import com.romand.gestiondestockV1.dtos.LigneCommandeFournisseurDto;
import com.romand.gestiondestockV1.services.LigneCommandeFournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class LigneCommandeFournisseurController implements LigneCommandeFournisseurApi {
    private final LigneCommandeFournisseurService ligneCommandeFournisseurService;
    @Autowired
    public LigneCommandeFournisseurController(LigneCommandeFournisseurService ligneCommandeFournisseurService) {
        this.ligneCommandeFournisseurService = ligneCommandeFournisseurService;
    }

    @Override
    public ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseur(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeFournisseurService.getAllLignesCommandeFournisseur(page, size));
    }
    @Override
    public ResponseEntity<LigneCommandeFournisseurDto> getLigneCommandeFournisseurById(String idLigneCommandeFournisseur) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeFournisseurService.getLigneCommandeFournisseurById(idLigneCommandeFournisseur));
    }
    @Override
    public ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseurByIdArticle(String idArticle, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeFournisseurService.getAllLignesCommandeFournisseurByIdArticle(idArticle, page, size));
    }
    @Override
    public ResponseEntity<Page<LigneCommandeFournisseurDto>> getAllLignesCommandeFournisseurByIdCommandeFournisseur(
            String idCommandeFopurnisseur, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ligneCommandeFournisseurService.getAllLignesCommandeFournisseurByIdCommandeFournisseur(
                        idCommandeFopurnisseur, page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneCommandeFournisseurDto> createLigneCommandeFournisseur(
            LigneCommandeFournisseurDto ligneCommandeFournisseurDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ligneCommandeFournisseurService.createLigneCommandeFournisseur(ligneCommandeFournisseurDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<LigneCommandeFournisseurDto> updateLigneCommandeFournisseur(
            String idLigneCommandeFournisseur, LigneCommandeFournisseurDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ligneCommandeFournisseurService.updateLigneCommandeFournisseur(idLigneCommandeFournisseur, dto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteLigneCommandeFournisseurById(String idLigneCommandeFournisseur) {
        return ResponseEntity.status(HttpStatus.OK).body(
                ligneCommandeFournisseurService.deleteLigneCommandeFournisseur(idLigneCommandeFournisseur));
    }
}
