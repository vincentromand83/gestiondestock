package com.romand.gestiondestockV1.controllers;

import com.romand.gestiondestockV1.controllers.api.MouvementStockApi;
import com.romand.gestiondestockV1.dtos.MouvementStockDto;
import com.romand.gestiondestockV1.services.MouvementStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class MouvementStockController implements MouvementStockApi {
    private final MouvementStockService mouvementStockService;
    @Autowired
    public MouvementStockController(MouvementStockService mouvementStockService) {
        this.mouvementStockService = mouvementStockService;
    }

    @Override
    public ResponseEntity<Page<MouvementStockDto>> getAllMouvementsStock(int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(mouvementStockService.getAllMouvementsStock(page, size));
    }
    @Override
    public ResponseEntity<MouvementStockDto> getMouvementStockById(String idMouvementStock) {
        return ResponseEntity.status(HttpStatus.OK).body(mouvementStockService.getMouvementStockById(idMouvementStock));
    }
    @Override
    public ResponseEntity<Page<MouvementStockDto>> getAllMouvementsStockByIdArticle(String idArticle, int page, int size) {
        return ResponseEntity.status(HttpStatus.OK).body(mouvementStockService.getAllMouvementsStockByIdArticle(
                idArticle, page, size));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<MouvementStockDto> createMouvementStock(MouvementStockDto mouvementStockDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(mouvementStockService.createMouvementStock(mouvementStockDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<MouvementStockDto> updateMouvementStock(String idMouvementStock, MouvementStockDto mouvementStockDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(mouvementStockService.updateMouvementStock(
                idMouvementStock, mouvementStockDto));
    }
    @Override
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public ResponseEntity<String> deleteMouvementStockById(String idMouvementStock) {
        return ResponseEntity.status(HttpStatus.OK).body(mouvementStockService.deleteMouvementStockById(idMouvementStock));
    }
}
