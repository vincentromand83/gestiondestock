package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.MouvementStockDto;
import com.romand.gestiondestockV1.entities.MouvementStock;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MouvementStockMapper {

    MouvementStock mouvementStockDtoToMouvementStock(MouvementStockDto mouvementStockDto);

    MouvementStockDto mouvementStockToMouvementStockDto(MouvementStock mouvementStock);
}
