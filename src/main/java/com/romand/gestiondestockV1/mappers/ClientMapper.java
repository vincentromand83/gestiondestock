package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.ClientDto;
import com.romand.gestiondestockV1.entities.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    Client clientDtoToClient(ClientDto clientDto);

    ClientDto cientToClientDto(Client client);
}
