package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.VenteDto;
import com.romand.gestiondestockV1.entities.Vente;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VenteMapper {

    Vente venteDtoToVente(VenteDto venteDto);

    VenteDto venteToVenteDto(Vente vente);
}
