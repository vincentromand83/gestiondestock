package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.CategoryDto;
import com.romand.gestiondestockV1.entities.Category;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    Category categoryDtoToCategory(CategoryDto categoryDto);

    CategoryDto categoryToCategoryDto(Category category);
}