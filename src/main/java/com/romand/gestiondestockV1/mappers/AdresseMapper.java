package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.AdresseDto;
import com.romand.gestiondestockV1.entities.Adresse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AdresseMapper {

    Adresse adresseDtoToAdresse(Adresse adresse);

    AdresseDto adresseToAdresseDto(AdresseDto adresseDto);
}
