package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.FournisseurDto;
import com.romand.gestiondestockV1.entities.Fournisseur;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FournisseurMapper {

    Fournisseur fournisseurDtoToFournisseur(FournisseurDto fournisseurDto);

    FournisseurDto fournisseurToFournisseurDto(Fournisseur fournisseur);
}
