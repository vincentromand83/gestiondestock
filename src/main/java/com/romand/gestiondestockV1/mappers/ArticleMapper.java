package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.ArticleDto;
import com.romand.gestiondestockV1.entities.Article;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ArticleMapper {

    Article articleDtoToArticle(ArticleDto articleDto);

    ArticleDto articleToArticleDto(Article article);
}
