package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import com.romand.gestiondestockV1.entities.Utilisateur;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UtilisateurMapper {

    Utilisateur utilisateurDtoToUtilisateur(UtilisateurDto utilisateurDto);

    UtilisateurDto utilisateurToUtilisateurDto(Utilisateur utilisateur);
}
