package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.RoleDto;
import com.romand.gestiondestockV1.entities.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    Role roleDtoToRole(RoleDto roleDto);

    RoleDto roleToRoleDto(Role role);
}
