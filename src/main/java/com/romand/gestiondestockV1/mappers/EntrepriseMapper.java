package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import com.romand.gestiondestockV1.entities.Entreprise;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EntrepriseMapper {

    Entreprise entrepriseDtoToentreprise(EntrepriseDto entrepriseDto);

    EntrepriseDto entrepriseToEntrepriseDto(Entreprise entreprise);
}