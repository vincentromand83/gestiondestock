package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.LigneCommandeFournisseurDto;
import com.romand.gestiondestockV1.entities.LigneCommandeFournisseur;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LigneCommandeFournisseurMapper {

    LigneCommandeFournisseur ligneCommandeFournisseurDtoToLigneCommandeFournisseur(LigneCommandeFournisseurDto ligneCommandeFournisseurDto);

    LigneCommandeFournisseurDto ligneCommandeFournisseurToLigneCommandeFournisseurDto(LigneCommandeFournisseur ligneCommandeFournisseur);
}
