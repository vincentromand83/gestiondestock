package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.LigneCommandeClientDto;
import com.romand.gestiondestockV1.entities.LigneCommandeClient;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LigneCommandeClientMapper {

    LigneCommandeClient ligneCommandeClientDtoToLigneCommandeClient(LigneCommandeClientDto ligneCommandeClientDto);

    LigneCommandeClientDto ligneCommandeClientToLigneCommandeClietDto(LigneCommandeClient ligneCommandeClient);
}