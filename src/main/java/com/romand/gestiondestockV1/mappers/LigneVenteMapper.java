package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.LigneVenteDto;
import com.romand.gestiondestockV1.entities.LigneVente;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LigneVenteMapper {

    LigneVente ligneVenteDtoToLigneVente(LigneVenteDto ligneVenteDto);

    LigneVenteDto ligneVenteToLigneVenteDto(LigneVente ligneVente);
}
