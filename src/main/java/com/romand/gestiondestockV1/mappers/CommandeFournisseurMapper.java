package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.CommandeFournisseurDto;
import com.romand.gestiondestockV1.entities.CommandeFournisseur;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommandeFournisseurMapper {

    CommandeFournisseur commandeFournisseurDtotoCommandeFournisseur(CommandeFournisseurDto commandeFournisseurDto);

    CommandeFournisseurDto commandeFournisseurToCommandeFournisseurDto(CommandeFournisseur commandeFournisseur);
}