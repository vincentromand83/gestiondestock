package com.romand.gestiondestockV1.mappers;

import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import com.romand.gestiondestockV1.entities.CommandeClient;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommandeClientMapper {

    CommandeClient commandeClientDtoToCommandeClient(CommandeClientDto commandeClientDto);

    CommandeClientDto commandeClientToCommandeClientDto(CommandeClient commandeClient);
}
