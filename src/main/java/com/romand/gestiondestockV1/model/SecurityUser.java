package com.romand.gestiondestockV1.model;

import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;

public class SecurityUser implements UserDetails {

    private final UtilisateurDto utilisateurDto;

    public SecurityUser(UtilisateurDto utilisateurDto) {
        this.utilisateurDto = utilisateurDto;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return utilisateurDto.getRoles().stream()
                .map(roleDto -> new SimpleGrantedAuthority(roleDto.getRoleName()))
                .toList();
    }

    @Override
    public String getPassword() {
        return utilisateurDto.getPassword();
    }

    @Override
    public String getUsername() {
        return utilisateurDto.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
