package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.VenteDto;
import org.springframework.data.domain.Page;

public interface VenteService {

    VenteDto getVenteById(String idVente);

    Page<VenteDto> getAllVentes(int page, int size);

    VenteDto createVente(VenteDto venteDto);

    VenteDto updateVente(String idVente, VenteDto venteDto);

    String deleteVenteById(String idVente);
}
