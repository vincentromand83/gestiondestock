package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import org.springframework.data.domain.Page;

public interface EntrepriseService {

    EntrepriseDto getEntrepriseById(String idEntreprise);

    Page<EntrepriseDto> getAllEntreprises(int page, int size);

    EntrepriseDto createEntreprise(EntrepriseDto entrepriseDto);

    EntrepriseDto updateEntreprise(String idEntreprise, EntrepriseDto entrepriseDto);

    String deleteEntrepriseById(String idEntreprise);

}
