package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import java.io.InputStream;

public interface Strategy<T> {

    T savePhoto(String id, InputStream photo, String title) throws FlickrException;

}
