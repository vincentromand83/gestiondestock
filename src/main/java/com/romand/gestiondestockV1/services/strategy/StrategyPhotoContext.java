package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import lombok.Setter;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.InputStream;

@Service
public class StrategyPhotoContext {

    private final BeanFactory beanFactory;
    private Strategy strategy;
    @Setter
    private  String context;
    @Autowired
    public StrategyPhotoContext(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public Object savePhoto(String context, String id, InputStream photo, String title) throws FlickrException {
        determinContext(context);
        return strategy.savePhoto(id, photo, title);
    }

    public void determinContext(String context) {
        final String beanName = strategy + "Service";
        switch (context) {
            case "article" -> strategy = beanFactory.getBean(beanName, SaveArticlePhoto.class);
            case "client" -> strategy = beanFactory.getBean(beanName, SaveClientPhoto.class);
            case "entreprise" -> strategy = beanFactory.getBean(beanName, SaveEntreprisePhoto.class);
            case "fournisseur" -> strategy = beanFactory.getBean(beanName, SaveFournisseurPhoto.class);
            case "utilisateur" -> strategy = beanFactory.getBean(beanName, SaveUtilisateurPhoto.class);
            default -> throw new InvalidOperationException(
                    "Contexte inconnue pour l'enregistrement de la photo", ErrorCodes.UNKNOWN_CONTEXT);
        }
    }
}
