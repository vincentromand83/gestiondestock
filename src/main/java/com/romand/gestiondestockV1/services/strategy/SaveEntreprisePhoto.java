package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.services.EntrepriseService;
import com.romand.gestiondestockV1.services.FlickrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.InputStream;

@Service("entrepriseService")
@Slf4j
public class SaveEntreprisePhoto implements Strategy<EntrepriseDto> {

    private final FlickrService flickrService;
    private final EntrepriseService entrepriseService;
    @Autowired
    public SaveEntreprisePhoto(FlickrService flickrService, EntrepriseService entrepriseService) {
        this.flickrService = flickrService;
        this.entrepriseService = entrepriseService;
    }

    @Override
    public EntrepriseDto savePhoto(String idEntreprise, InputStream photo, String title) throws FlickrException {
        EntrepriseDto entreprise = entrepriseService.getEntrepriseById(idEntreprise);
        String urlPhoto = flickrService.savePhoto(photo, title);
        if(!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException(
                    "Erreur lors de l'enregistrement de la photo de l'entreprise", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        entreprise.setPhoto(urlPhoto);
        return entrepriseService.createEntreprise(entreprise);
    }
}
