package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.dtos.ClientDto;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.services.ClientService;
import com.romand.gestiondestockV1.services.FlickrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.InputStream;

@Service("clientService")
@Slf4j
public class SaveClientPhoto implements Strategy<ClientDto> {

    private final FlickrService flickrService;
    private final ClientService clientService;
    @Autowired
    public SaveClientPhoto(FlickrService flickrService, ClientService clientService) {
        this.flickrService = flickrService;
        this.clientService = clientService;
    }

    @Override
    public ClientDto savePhoto(String idClient, InputStream photo, String title) throws FlickrException {
        ClientDto client = clientService.getClientById(idClient);
        String urlPhoto = flickrService.savePhoto(photo, title);
        if(!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException(
                    "Erreur lors de l'enregistrement de la photo du client", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        client.setPhoto(urlPhoto);
        return clientService.createClient(client);
    }
}
