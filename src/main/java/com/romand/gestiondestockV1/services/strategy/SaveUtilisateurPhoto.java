package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.services.FlickrService;
import com.romand.gestiondestockV1.services.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.InputStream;

@Service("utilisateurService")
@Slf4j
public class SaveUtilisateurPhoto implements Strategy<UtilisateurDto> {

    private final FlickrService flickrService;
    private final UtilisateurService utilisateurService;
    @Autowired
    public SaveUtilisateurPhoto(FlickrService flickrService, UtilisateurService utilisateurService) {
        this.flickrService = flickrService;
        this.utilisateurService = utilisateurService;
    }

    @Override
    public UtilisateurDto savePhoto(String idUtilisateur, InputStream photo, String title) throws FlickrException {
        UtilisateurDto utilisateur = utilisateurService.getUtilisateurById(idUtilisateur);
        String urlPhoto = flickrService.savePhoto(photo, title);
        if(!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException(
                    "Erreur lors de l'enregistrement de la photo de l'utilisateur", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        utilisateur.setPhoto(urlPhoto);
        return utilisateurService.createUtilisateur(utilisateur);
    }
}
