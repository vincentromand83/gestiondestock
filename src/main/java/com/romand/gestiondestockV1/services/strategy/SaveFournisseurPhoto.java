package com.romand.gestiondestockV1.services.strategy;

import com.flickr4java.flickr.FlickrException;
import com.romand.gestiondestockV1.dtos.FournisseurDto;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.services.FlickrService;
import com.romand.gestiondestockV1.services.FournisseurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.InputStream;

@Service("fournisseurService")
@Slf4j
public class SaveFournisseurPhoto implements Strategy<FournisseurDto> {

    private final FlickrService flickrService;
    private final FournisseurService fournisseurService;
    @Autowired
    public SaveFournisseurPhoto(FlickrService flickrService, FournisseurService fournisseurService) {
        this.flickrService = flickrService;
        this.fournisseurService = fournisseurService;
    }

    @Override
    public FournisseurDto savePhoto(String idFournisseur, InputStream photo, String title) throws FlickrException {
        FournisseurDto fournisseur = fournisseurService.getFournisseurById(idFournisseur);
        String urlPhoto = flickrService.savePhoto(photo, title);
        if(!StringUtils.hasLength(urlPhoto)) {
            throw new InvalidOperationException(
                    "Erreur lors de l'enregistrement de la photo du fournisseur", ErrorCodes.UPDATE_PHOTO_EXCEPTION);
        }
        fournisseur.setPhoto(urlPhoto);
        return fournisseurService.createFournisseur(fournisseur);
    }
}
