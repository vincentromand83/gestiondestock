package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.LigneCommandeClientDto;
import org.springframework.data.domain.Page;

public interface LigneCommandeClientService {

    LigneCommandeClientDto getLigneCommandeClientById(String idLigneCommandeClient);

    Page<LigneCommandeClientDto> getAllLignesCommandesClient(int page, int size);

    Page<LigneCommandeClientDto> getAllLignesCommandeClientByIdArticle(String idArticle, int page, int size);

    Page<LigneCommandeClientDto> getAllLignesCommandeClientByIdCommandeClient(String idCommandeClient, int page, int size);

    LigneCommandeClientDto createLigneCommandeClient(LigneCommandeClientDto ligneCommandeClientDto);

    LigneCommandeClientDto updateLigneCommandeClient(String idLigneCommandeClient,
                                                     LigneCommandeClientDto ligneCommandeClientDto);

    String deleteLigneCommandeClientById(String idLigneCommandeClient);
}
