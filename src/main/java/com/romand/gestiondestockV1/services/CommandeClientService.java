package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import org.springframework.data.domain.Page;

public interface CommandeClientService {

    CommandeClientDto getCommandeClientById(String idCommandeClient);

    Page<CommandeClientDto> getAllCommandesClient(int page, int size);

    Page<CommandeClientDto> getAllCommandesClientByIdClient(String idClient, int page, int size);

    CommandeClientDto createCommandeClient(CommandeClientDto commandeClientDto);

    CommandeClientDto updateCommandeClient(String idCommandeClient, CommandeClientDto commandeClientDto);

    String deleteCommandeClient(String idCommandeClient);
}
