package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.ArticleDto;
import org.springframework.data.domain.Page;

public interface ArticleService {

    ArticleDto getArticleById(String idArticle);

    Page<ArticleDto> getAllArticles(int page, int size);

    Page<ArticleDto> getAllArticlesByIdCategory(String idCategory, int page, int size);

    Page<ArticleDto> getAllArticlesByIdEntreprise(String idEntreprise, int page, int size);

    ArticleDto createArticle(ArticleDto articleDto);

    ArticleDto updateArticle(String idArticle, ArticleDto articleDto);

    String deleteArticleById(String idArticle);
}
