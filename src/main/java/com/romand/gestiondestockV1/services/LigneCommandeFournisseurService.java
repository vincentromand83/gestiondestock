package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.LigneCommandeFournisseurDto;
import org.springframework.data.domain.Page;

public interface LigneCommandeFournisseurService {

    LigneCommandeFournisseurDto getLigneCommandeFournisseurById(String idLigneCommandeFournisseur);

    Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseur(int page, int size);

    Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseurByIdArticle(String idArticle, int page, int size);

    Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseurByIdCommandeFournisseur(
            String idCommandeFournisseur, int page, int size);

    LigneCommandeFournisseurDto createLigneCommandeFournisseur(LigneCommandeFournisseurDto ligneCommandeFournisseurDto);

    LigneCommandeFournisseurDto updateLigneCommandeFournisseur(
            String idLigneCommandeFournisseur, LigneCommandeFournisseurDto ligneCommandeFournisseurDto);

    String deleteLigneCommandeFournisseur(String idLigneCommandeFournisseur);
}
