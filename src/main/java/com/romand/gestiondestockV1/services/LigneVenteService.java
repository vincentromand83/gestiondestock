package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.LigneVenteDto;
import org.springframework.data.domain.Page;

public interface LigneVenteService {

    LigneVenteDto getLigneVenteById(String idLigneVente);

    Page<LigneVenteDto> getAllLignesVente(int page, int size);

    Page<LigneVenteDto> getAllLignesVenteByIdArticle(String idArticle, int page, int size);

    Page<LigneVenteDto> getAllLignesVenteByIdVente(String idVente, int page, int size);

    LigneVenteDto createLigneVente(LigneVenteDto ligneVenteDto);

    LigneVenteDto updateLigneVente(String idLigneVente, LigneVenteDto ligneVenteDto);

    String deleteLigneVenteById(String idLigneVente);
}
