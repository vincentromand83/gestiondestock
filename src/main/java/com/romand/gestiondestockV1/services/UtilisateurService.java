package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import org.springframework.data.domain.Page;

public interface UtilisateurService {

    UtilisateurDto getUtilisateurById(String idUtilisateur);

    Page<UtilisateurDto> getAllUtilisateurs(int page, int size);

    Page<UtilisateurDto> getAllUtilisateursByIdEntreprise(String idEntreprise, int page, int size);

    UtilisateurDto createUtilisateur(UtilisateurDto utilisateurDto);

    UtilisateurDto updateUtilisateur(String idUtilisateur, UtilisateurDto utilisateurDto);

    String deleteUtilisateurById(String idUtilisateur);
}
