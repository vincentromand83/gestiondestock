package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.CategoryDto;
import org.springframework.data.domain.Page;

public interface CategoryService {

    CategoryDto getCategoryById(String idCategory);

    Page<CategoryDto> getAllCategories(int page, int size);

    CategoryDto createCategory(CategoryDto categoryDto);

    CategoryDto updateCategory(String idCategory, CategoryDto categoryDto);

    String deleteCategoryById(String idCategory);
}