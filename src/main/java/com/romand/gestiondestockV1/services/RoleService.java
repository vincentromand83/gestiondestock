package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.RoleDto;
import org.springframework.data.domain.Page;

public interface RoleService {

    RoleDto getRoleById(String idRole);

    Page<RoleDto> getAllRoles(int page, int size);

    RoleDto createRole(RoleDto roleDto);

    RoleDto updateRole(String idRole, RoleDto roleDto);

    String deleteRoleById(String idRole);
}
