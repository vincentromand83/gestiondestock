package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.FournisseurDto;
import org.springframework.data.domain.Page;

public interface FournisseurService {

    FournisseurDto getFournisseurById(String idFournisseur);

    Page<FournisseurDto> getAllFournisseurs(int page, int size);

    FournisseurDto createFournisseur(FournisseurDto fournisseurDto);

    FournisseurDto updateFournisseur(String idFournisseur, FournisseurDto fournisseurDto);

    String deleteFournisseurById(String idFournisseur);
}
