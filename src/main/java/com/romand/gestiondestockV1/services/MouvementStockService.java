package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.MouvementStockDto;
import org.springframework.data.domain.Page;

public interface MouvementStockService {

    MouvementStockDto getMouvementStockById(String idMouvementStock);

    Page<MouvementStockDto> getAllMouvementsStock(int page, int size);

    Page<MouvementStockDto> getAllMouvementsStockByIdArticle(String idArticle, int page, int size);

    MouvementStockDto createMouvementStock(MouvementStockDto mouvementStockDto);

    MouvementStockDto updateMouvementStock(String idMouvementStock, MouvementStockDto mouvementStockDto);

    String deleteMouvementStockById(String idMouvementStock);
}
