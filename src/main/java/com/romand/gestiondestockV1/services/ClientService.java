package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.ClientDto;
import org.springframework.data.domain.Page;

public interface ClientService {

    ClientDto getClientById(String idClient);

    Page<ClientDto> getAllClients(int page, int size);

    ClientDto createClient(ClientDto clientDto);

    ClientDto updateClient(String idClient, ClientDto clientDto);

    String deleteClientById(String idClient);
}
