package com.romand.gestiondestockV1.services;

import com.romand.gestiondestockV1.dtos.CommandeFournisseurDto;
import org.springframework.data.domain.Page;

public interface CommandeFournisseurService {

    CommandeFournisseurDto getCommandeFournisseurById(String idCommandeFournisseur);

    Page<CommandeFournisseurDto> getAllCommandesFournisseur(int page, int size);

    Page<CommandeFournisseurDto> getAllCommandesFournisseurByIdFournisseur(String idFournisseur, int page, int size);

    CommandeFournisseurDto createCommandeFournisseur(CommandeFournisseurDto commandeFournisseurDto);

    CommandeFournisseurDto updateCommandeFournisseur(String idCommandeFournisseur, CommandeFournisseurDto commandeFournisseurDto);

    String deleteCommandeFournisseurById(String idCommandeFournisseur);
}
