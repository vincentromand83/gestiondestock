package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.CommandeFournisseurDto;
import com.romand.gestiondestockV1.entities.LigneCommandeFournisseur;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.CommandeFournisseurMapper;
import com.romand.gestiondestockV1.repositories.CommandeFournisseurRepository;
import com.romand.gestiondestockV1.repositories.LigneCommandeFournisseurRepository;
import com.romand.gestiondestockV1.services.CommandeFournisseurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class CommandeFournisseurImpl implements CommandeFournisseurService {
    private final CommandeFournisseurRepository commandeFournisseurRepository;
    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;

    private final CommandeFournisseurMapper commandeFournisseurMapper;

    public CommandeFournisseurImpl(CommandeFournisseurRepository commandeFournisseurRepository,
                                   LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                                   CommandeFournisseurMapper commandeFournisseurMapper) {
        this.commandeFournisseurRepository = commandeFournisseurRepository;
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.commandeFournisseurMapper = commandeFournisseurMapper;
    }

    @Override
    public CommandeFournisseurDto getCommandeFournisseurById(String idCommandeFournisseur) {
        return commandeFournisseurRepository.findById(idCommandeFournisseur)
                .map(commandeFournisseurMapper::commandeFournisseurToCommandeFournisseurDto).orElseThrow(()->
                        new EntityNotFoundException(
                                "Aucune commande fournisseur avec l'ID "
                                        +idCommandeFournisseur+" n'a ete trouve dans la BDD",
                                ErrorCodes.COMMANDE_FOURNISSEUR_NOT_FOUND)
                );
    }
    @Override
    public Page<CommandeFournisseurDto> getAllCommandesFournisseur(int page, int size) {
        return commandeFournisseurRepository.findAll(PageRequest.of(page, size))
                .map(commandeFournisseurMapper::commandeFournisseurToCommandeFournisseurDto);
    }
    @Override
    public Page<CommandeFournisseurDto> getAllCommandesFournisseurByIdFournisseur(String idFournisseur, int page, int size) {
        return commandeFournisseurRepository.findByFournisseurIdFournisseur(idFournisseur, PageRequest.of(page, size))
                .map(commandeFournisseurMapper::commandeFournisseurToCommandeFournisseurDto);
    }
    @Override
    public CommandeFournisseurDto createCommandeFournisseur(CommandeFournisseurDto commandeFournisseurDto) {
        return commandeFournisseurMapper.commandeFournisseurToCommandeFournisseurDto(commandeFournisseurRepository.save(
                commandeFournisseurMapper.commandeFournisseurDtotoCommandeFournisseur(commandeFournisseurDto)));
    }
    @Override
    public CommandeFournisseurDto updateCommandeFournisseur(String idCommandeFournisseur,
                                                            CommandeFournisseurDto commandeFournisseurDto) {
        var commandeFournisseur = commandeFournisseurMapper.commandeFournisseurDtotoCommandeFournisseur(
                getCommandeFournisseurById(idCommandeFournisseur));
        commandeFournisseur = commandeFournisseurMapper.commandeFournisseurDtotoCommandeFournisseur(commandeFournisseurDto);
        commandeFournisseur.setIdCommandeFournisseur(idCommandeFournisseur);
        return createCommandeFournisseur(commandeFournisseurMapper
                .commandeFournisseurToCommandeFournisseurDto(commandeFournisseur));
    }
    @Override
    public String deleteCommandeFournisseurById(String idCommandeFournisseur) {
        List<LigneCommandeFournisseur> lignesCommandeFournisseur = ligneCommandeFournisseurRepository
                .findByCommandeFournisseurIdCommandeFournisseur(idCommandeFournisseur);
        if (!lignesCommandeFournisseur.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une commande présente dans une ligne de commande",
                    ErrorCodes.COMMANDE_FOURNISSEUR_ALREADY_IN_USE);
        }
        commandeFournisseurRepository.delete(commandeFournisseurMapper.commandeFournisseurDtotoCommandeFournisseur(
                getCommandeFournisseurById(idCommandeFournisseur)));
        return "La commande fournisseur a bien été supprimée";
    }
}
