package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.RoleDto;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.mappers.RoleMapper;
import com.romand.gestiondestockV1.repositories.RoleRepository;
import com.romand.gestiondestockV1.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    private final RoleMapper roleMapper;

    public RoleServiceImpl(RoleRepository roleRepository, RoleMapper roleMapper) {
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
    }

    @Override
    public RoleDto getRoleById(String idRole) {
        return roleRepository.findById(idRole).map(roleMapper::roleToRoleDto).orElseThrow(()->
                new EntityNotFoundException("Aucun role avec l'ID "+idRole+" n'a été trouvé dans la BDD",
                        ErrorCodes.ROLE_NOT_FOUND));
    }
    @Override
    public Page<RoleDto> getAllRoles(int page, int size) {
        return roleRepository.findAll(PageRequest.of(page, size)).map(roleMapper::roleToRoleDto);
    }
    @Override
    public RoleDto createRole(RoleDto roleDto) {
        return roleMapper.roleToRoleDto(roleRepository.save(roleMapper.roleDtoToRole(roleDto)));
    }
    @Override
    public RoleDto updateRole(String idRole, RoleDto roleDto) {
        var role = roleMapper.roleDtoToRole(getRoleById(idRole));
        role = roleMapper.roleDtoToRole(roleDto);
        role.setIdRole(idRole);
        return createRole(roleMapper.roleToRoleDto(role));
    }
    @Override
    public String deleteRoleById(String idRole) {
        roleRepository.delete(roleMapper.roleDtoToRole(getRoleById(idRole)));
        return "Le role a bien été supprimé";
    }
}
