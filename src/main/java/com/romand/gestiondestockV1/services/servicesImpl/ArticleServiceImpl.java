package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.ArticleDto;
import com.romand.gestiondestockV1.entities.LigneCommandeClient;
import com.romand.gestiondestockV1.entities.LigneCommandeFournisseur;
import com.romand.gestiondestockV1.entities.LigneVente;
import com.romand.gestiondestockV1.entities.MouvementStock;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.ArticleMapper;
import com.romand.gestiondestockV1.repositories.*;
import com.romand.gestiondestockV1.services.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository articleRepository;
    private final LigneCommandeClientRepository ligneCommandeClientRepository;
    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;
    private final LigneVenteRepository ligneVenteRepository;
    private final MouvementStockRepository mouvementStockRepository;
    private final ArticleMapper articleMapper;

    public ArticleServiceImpl(ArticleRepository articleRepository,
                              LigneCommandeClientRepository ligneCommandeClientRepository,
                              LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                              LigneVenteRepository ligneVenteRepository, ArticleMapper articleMapper,
                              MouvementStockRepository mouvementStockRepository) {
        this.articleRepository = articleRepository;
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.ligneVenteRepository = ligneVenteRepository;
        this.articleMapper = articleMapper;
        this.mouvementStockRepository = mouvementStockRepository;
    }

    @Override
    public ArticleDto getArticleById(String idArticle) {
        return articleRepository.findById(idArticle).map(articleMapper::articleToArticleDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucun article avec l'Id "+ idArticle + " n'a été trouvé dans la BDD",
                        ErrorCodes.ARTICLE_NOT_FOUND)
        );
    }
    @Override
    public Page<ArticleDto> getAllArticles(int page, int size) {
        return articleRepository.findAll(PageRequest.of(page, size)).map(articleMapper::articleToArticleDto);
    }
    @Override
    public Page<ArticleDto> getAllArticlesByIdCategory(String idCategory, int page, int size) {
        return articleRepository.findByCategoryIdCategory(idCategory, PageRequest.of(page, size))
                .map(articleMapper::articleToArticleDto);
    }
    @Override
    public Page<ArticleDto> getAllArticlesByIdEntreprise(String idEntreprise, int page, int size) {
        return articleRepository.findByEntrepriseIdEntreprise(idEntreprise, PageRequest.of(page, size))
                .map(articleMapper::articleToArticleDto);
    }
    @Override
    public ArticleDto createArticle(ArticleDto articleDto) {
        return articleMapper.articleToArticleDto(articleRepository.save(articleMapper.articleDtoToArticle(articleDto)));
    }
    @Override
    public ArticleDto updateArticle(String idArticle, ArticleDto articleDto) {
        var article = articleMapper.articleDtoToArticle(getArticleById(idArticle));
        article = articleMapper.articleDtoToArticle(articleDto);
        article.setIdArticle(idArticle);
        return createArticle(articleMapper.articleToArticleDto(article));
    }
    @Override
    public String deleteArticleById(String idArticle) {
       List<LigneCommandeClient> ligneCommandeClients = ligneCommandeClientRepository.findByArticleIdArticle(idArticle);
       if(!ligneCommandeClients.isEmpty()) {
           throw new InvalidOperationException("Impossible de supprimer un article déjà utilisé dans une commande",
                   ErrorCodes.ARTICLE_ALREADY_IN_USE);
       }
        List<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommandeFournisseurRepository
                .findByArticleIdArticle(idArticle);
        if(!ligneCommandeFournisseurs.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer un article déjà utilisé dans une commande",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
       List<LigneVente> ligneVentes = ligneVenteRepository.findByArticleIdArticle(idArticle);
       if(!ligneVentes.isEmpty()) {
           throw new InvalidOperationException("Impossible de supprimer un article déjà utilisé dans une vente",
                   ErrorCodes.ARTICLE_ALREADY_IN_USE);
       }
       List<MouvementStock> mouvementStocks = mouvementStockRepository.findByArticleIdArticle(idArticle);
       if (!mouvementStocks.isEmpty()) {
           throw new InvalidOperationException("Impossible de supprimer un article affecté par un mouvement de stock",
                   ErrorCodes.ARTICLE_ALREADY_IN_USE);
       }
       articleRepository.delete(articleMapper.articleDtoToArticle(getArticleById(idArticle)));
       return "L'article a bien été supprimé";
    }
}
