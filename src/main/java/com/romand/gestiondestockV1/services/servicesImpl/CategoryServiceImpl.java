package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.CategoryDto;
import com.romand.gestiondestockV1.entities.Article;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.CategoryMapper;
import com.romand.gestiondestockV1.repositories.ArticleRepository;
import com.romand.gestiondestockV1.repositories.CategoryRepository;
import com.romand.gestiondestockV1.services.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ArticleRepository articleRepository;

    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, ArticleRepository articleRepository,
                               CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.articleRepository = articleRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public CategoryDto getCategoryById(String idCategory) {
        return categoryRepository.findById(idCategory).map(categoryMapper::categoryToCategoryDto).orElseThrow(()->
                new EntityNotFoundException(
                    "Aucune catégorie avec l'ID "+idCategory+" na été trouvé dans la BDD",
                     ErrorCodes.CATEGORY_NOT_FOUND)
        );
    }
    @Override
    public Page<CategoryDto> getAllCategories(int page, int size) {
        return categoryRepository.findAll(PageRequest.of(page, size)).map(categoryMapper::categoryToCategoryDto);
    }
    @Override
    public CategoryDto createCategory(CategoryDto categoryDto) {

        return categoryMapper.categoryToCategoryDto(categoryRepository.save(categoryMapper.categoryDtoToCategory(categoryDto)));
    }
    @Override
    public CategoryDto updateCategory(String idCategory, CategoryDto categoryDto) {
        var category = categoryMapper.categoryDtoToCategory(getCategoryById(idCategory));
        category = categoryMapper.categoryDtoToCategory(categoryDto);
        category.setIdCategory(idCategory);
        return createCategory(categoryMapper.categoryToCategoryDto(category));
    }
    @Override
    public String deleteCategoryById(String idCategory) {
        var category = getCategoryById(idCategory);
        List<Article> articles = articleRepository.findByCategoryIdCategory(idCategory);
        if (!articles.isEmpty()) {
            throw new InvalidOperationException(
                    "Impossible de supprimer une catégorie utilisée dans un article",
                    ErrorCodes.CATEGORY_ALREADY_IN_USE
            );
        }
        categoryRepository.delete(categoryMapper.categoryDtoToCategory(getCategoryById(idCategory)));
        return "La catégorie a bien été supprimé";
    }
}
