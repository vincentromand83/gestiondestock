package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.UtilisateurDto;
import com.romand.gestiondestockV1.entities.Role;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.UtilisateurMapper;
import com.romand.gestiondestockV1.repositories.RoleRepository;
import com.romand.gestiondestockV1.repositories.UtilisateurRepository;
import com.romand.gestiondestockV1.services.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class UtilisateurServiceImpl implements UtilisateurService {
    private final UtilisateurRepository utilisateurRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;
    private final UtilisateurMapper utilisateurMapper;

    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository, RoleRepository roleRepository,
                                  UtilisateurMapper utilisateurMapper, PasswordEncoder encoder) {
        this.utilisateurRepository = utilisateurRepository;
        this.roleRepository = roleRepository;
        this.utilisateurMapper = utilisateurMapper;
        this.encoder = encoder;
    }

    @Override
    public UtilisateurDto getUtilisateurById(String idUtilisateur) {
        return utilisateurRepository.findById(idUtilisateur)
                .map(utilisateurMapper::utilisateurToUtilisateurDto).orElseThrow(()->
                new EntityNotFoundException("Aucun utilisateur avec l'ID "+idUtilisateur+" n'a été trouvé dans la BDD",
                        ErrorCodes.UTILISATEUR_NOT_FOUND));
    }
    @Override
    public Page<UtilisateurDto> getAllUtilisateurs(int page, int size) {
        return utilisateurRepository.findAll(PageRequest.of(page, size)).map(utilisateurMapper::utilisateurToUtilisateurDto);
    }
    @Override
    public Page<UtilisateurDto> getAllUtilisateursByIdEntreprise(String idEntreprise, int page, int size) {
        return utilisateurRepository.findByEntrepriseIdEntreprise(
                idEntreprise, PageRequest.of(page, size)).map(utilisateurMapper::utilisateurToUtilisateurDto);
    }
    @Override
    public UtilisateurDto createUtilisateur(UtilisateurDto utilisateurDto) {
        utilisateurDto.setPassword(encoder.encode(utilisateurDto.getPassword()));
        return utilisateurMapper.utilisateurToUtilisateurDto(utilisateurRepository
                .save(utilisateurMapper.utilisateurDtoToUtilisateur(utilisateurDto)));
    }
    @Override
    public UtilisateurDto updateUtilisateur(String idUtilisateur, UtilisateurDto utilisateurDto) {
        var utilisateur = utilisateurMapper.utilisateurDtoToUtilisateur(getUtilisateurById(idUtilisateur));
        utilisateur = utilisateurMapper.utilisateurDtoToUtilisateur(utilisateurDto);
        utilisateur.setIdUtilisateur(idUtilisateur);
        return createUtilisateur(utilisateurMapper.utilisateurToUtilisateurDto(utilisateur));
    }
    @Override
    public String deleteUtilisateurById(String idUtilisateur) {
        utilisateurRepository.delete(utilisateurMapper.utilisateurDtoToUtilisateur(getUtilisateurById(idUtilisateur)));
        return "L'utilisateur a bien été supprimé avec succés";
    }
}
