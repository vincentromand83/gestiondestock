package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.EntrepriseDto;
import com.romand.gestiondestockV1.entities.Article;
import com.romand.gestiondestockV1.entities.Utilisateur;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.EntrepriseMapper;
import com.romand.gestiondestockV1.repositories.ArticleRepository;
import com.romand.gestiondestockV1.repositories.EntrepriseRepository;
import com.romand.gestiondestockV1.repositories.UtilisateurRepository;
import com.romand.gestiondestockV1.services.EntrepriseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class EntrepriseServiceImpl implements EntrepriseService {
    private final EntrepriseRepository entrepriseRepository;
    private final ArticleRepository articleRepository;
    private final UtilisateurRepository utilisateurRepository;

    private final EntrepriseMapper entrepriseMapper;

    public EntrepriseServiceImpl(EntrepriseRepository entrepriseRepository, ArticleRepository articleRepository,
                                 UtilisateurRepository utilisateurRepository, EntrepriseMapper entrepriseMapper) {
        this.entrepriseRepository = entrepriseRepository;
        this.articleRepository = articleRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.entrepriseMapper = entrepriseMapper;
    }
    @Override
    public EntrepriseDto getEntrepriseById(String idEntreprise) {
        return entrepriseRepository.findById(idEntreprise).map(entrepriseMapper::entrepriseToEntrepriseDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucune entreprise avec l'ID "+idEntreprise+" n'a été trouvé dans la BDD",
                        ErrorCodes.ENTREPRISE_NOT_FOUND
                )
        );
    }
    @Override
    public Page<EntrepriseDto> getAllEntreprises(int page, int size) {
        return entrepriseRepository.findAll(PageRequest.of(page, size)).map(entrepriseMapper::entrepriseToEntrepriseDto);
    }
    @Override
    public EntrepriseDto createEntreprise(EntrepriseDto entrepriseDto) {
        return entrepriseMapper.entrepriseToEntrepriseDto(entrepriseRepository
                .save(entrepriseMapper.entrepriseDtoToentreprise(entrepriseDto)));
    }
    @Override
    public EntrepriseDto updateEntreprise(String idEntreprise, EntrepriseDto entrepriseDto) {
        var entreprise = entrepriseMapper.entrepriseDtoToentreprise(getEntrepriseById(idEntreprise));
        entreprise = entrepriseMapper.entrepriseDtoToentreprise(entrepriseDto);
        entreprise.setIdEntreprise(idEntreprise);
        return createEntreprise(entrepriseMapper.entrepriseToEntrepriseDto(entreprise));
    }
    @Override
    public String deleteEntrepriseById(String idEntreprise) {
        List<Article> articles = articleRepository.findByEntrepriseIdEntreprise(idEntreprise);
        if (!articles.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une entreprise avec des articles affectés",
                    ErrorCodes.ARTICLE_ALREADY_IN_USE);
        }
        List<Utilisateur> utilisateurs = utilisateurRepository.findByEntrepriseIdEntreprise(idEntreprise);
        if (!utilisateurs.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une entreprise avec des utilisateurs",
                    ErrorCodes.UTILISATEUR_ALREADY_EXISTS);
        }
        entrepriseRepository.delete(entrepriseMapper.entrepriseDtoToentreprise(getEntrepriseById(idEntreprise)));
        return "L'entreprise a bien été supprimé";
    }
}