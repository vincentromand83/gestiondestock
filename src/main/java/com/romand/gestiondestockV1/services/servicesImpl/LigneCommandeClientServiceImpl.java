package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.LigneCommandeClientDto;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.mappers.LigneCommandeClientMapper;
import com.romand.gestiondestockV1.repositories.LigneCommandeClientRepository;
import com.romand.gestiondestockV1.services.LigneCommandeClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LigneCommandeClientServiceImpl implements LigneCommandeClientService {
    private final LigneCommandeClientRepository ligneCommandeClientRepository;

    private final LigneCommandeClientMapper ligneCommandeClientMapper;

    public LigneCommandeClientServiceImpl(LigneCommandeClientRepository ligneCommandeClientRepository,
                                          LigneCommandeClientMapper ligneCommandeClientMapper) {
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.ligneCommandeClientMapper = ligneCommandeClientMapper;
    }
    @Override
    public LigneCommandeClientDto getLigneCommandeClientById(String idLigneCommandeClient) {
        return ligneCommandeClientRepository.findById(idLigneCommandeClient)
                .map(ligneCommandeClientMapper::ligneCommandeClientToLigneCommandeClietDto).orElseThrow(()->
                        new EntityNotFoundException(
                                "Aucune ligne commande client avec l'ID "+idLigneCommandeClient+" " +
                                        "n'a été trouvé dans la BDD", ErrorCodes.LIGNE_COMMANDE_CLIENT_NOT_FOUND));
    }
    @Override
    public Page<LigneCommandeClientDto> getAllLignesCommandesClient(int page, int size) {
        return ligneCommandeClientRepository.findAll(PageRequest.of(page, size))
                .map(ligneCommandeClientMapper::ligneCommandeClientToLigneCommandeClietDto);
    }
    @Override
    public Page<LigneCommandeClientDto> getAllLignesCommandeClientByIdArticle(String idArticle, int page, int size) {
        return ligneCommandeClientRepository.findByArticleIdArticle(idArticle, PageRequest.of(page, size))
                .map(ligneCommandeClientMapper::ligneCommandeClientToLigneCommandeClietDto);
    }
    @Override
    public Page<LigneCommandeClientDto> getAllLignesCommandeClientByIdCommandeClient(String idCommandeClient, int page, int size) {
        return ligneCommandeClientRepository.findByCommandeClientIdCommandeClient(
                idCommandeClient, PageRequest.of(page, size))
                .map(ligneCommandeClientMapper::ligneCommandeClientToLigneCommandeClietDto);
    }
    @Override
    public LigneCommandeClientDto createLigneCommandeClient(LigneCommandeClientDto ligneCommandeClientDto) {
        return ligneCommandeClientMapper.ligneCommandeClientToLigneCommandeClietDto(ligneCommandeClientRepository.save(
                ligneCommandeClientMapper.ligneCommandeClientDtoToLigneCommandeClient(ligneCommandeClientDto)));
    }
    @Override
    public LigneCommandeClientDto updateLigneCommandeClient(String idLigneCommandeClient, LigneCommandeClientDto ligneCommandeClientDto) {
        var ligneCommandeClient = ligneCommandeClientMapper
                .ligneCommandeClientDtoToLigneCommandeClient(getLigneCommandeClientById(idLigneCommandeClient));
        ligneCommandeClient = ligneCommandeClientMapper.ligneCommandeClientDtoToLigneCommandeClient(ligneCommandeClientDto);
        ligneCommandeClient.setIdLigneCommandeClient(idLigneCommandeClient);
        return createLigneCommandeClient(ligneCommandeClientMapper.ligneCommandeClientToLigneCommandeClietDto(ligneCommandeClient));
    }
    @Override
    public String deleteLigneCommandeClientById(String idLigneCommandeClient) {
        ligneCommandeClientRepository.delete(ligneCommandeClientMapper.ligneCommandeClientDtoToLigneCommandeClient(
                getLigneCommandeClientById(idLigneCommandeClient)));
        return "La ligne commande client a bien été supprimée";
    }
}
