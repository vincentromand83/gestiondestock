package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.ClientDto;
import com.romand.gestiondestockV1.entities.CommandeClient;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.ClientMapper;
import com.romand.gestiondestockV1.repositories.ClientRepository;
import com.romand.gestiondestockV1.repositories.CommandeClientRepository;
import com.romand.gestiondestockV1.services.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final CommandeClientRepository commandeClientRepository;

    private final ClientMapper clientMapper;

    public ClientServiceImpl(ClientRepository clientRepository, CommandeClientRepository commandeClientRepository,
                             ClientMapper clientMapper) {
        this.clientRepository = clientRepository;
        this.commandeClientRepository = commandeClientRepository;
        this.clientMapper = clientMapper;
    }

    @Override
    public ClientDto getClientById(String idClient) {
        return clientRepository.findById(idClient).map(clientMapper::cientToClientDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucun client avec l'ID "+idClient+" n'a été trouvé dans la BDD",
                        ErrorCodes.CLIENT_NOT_FOUND)
        );
    }
    @Override
    public Page<ClientDto> getAllClients(int page, int size) {
        return clientRepository.findAll(PageRequest.of(page, size)).map(clientMapper::cientToClientDto);
    }
    @Override
    public ClientDto createClient(ClientDto clientDto) {
        return clientMapper.cientToClientDto(clientRepository.save(clientMapper.clientDtoToClient(clientDto)));
    }
    @Override
    public ClientDto updateClient(String idClient, ClientDto clientDto) {
        var client = clientMapper.clientDtoToClient(getClientById(idClient));
        client = clientMapper.clientDtoToClient(clientDto);
        client.setIdClient(idClient);
        return createClient(clientMapper.cientToClientDto(client));
    }
    @Override
    public String deleteClientById(String idClient) {
        List<CommandeClient> commandeClients = commandeClientRepository.findByClientIdClient(idClient);
        if (!commandeClients.isEmpty()) {
            throw new InvalidOperationException(
                    "Impossible de supprimer un client qui a déjà passé une commande",
                    ErrorCodes.CLIENT_ALREADY_IN_USE
            );
        }
        clientRepository.delete(clientMapper.clientDtoToClient(getClientById(idClient)));
        return "Le client a bien été supprimé";
    }
}
