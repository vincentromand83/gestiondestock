package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.entities.Utilisateur;
import com.romand.gestiondestockV1.mappers.UtilisateurMapper;
import com.romand.gestiondestockV1.model.SecurityUser;
import com.romand.gestiondestockV1.repositories.UtilisateurRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;

    public JpaUserDetailsService(UtilisateurRepository utilisateurRepository,
                                 UtilisateurMapper utilisateurMapper) {
        this.utilisateurRepository = utilisateurRepository;
        this.utilisateurMapper = utilisateurMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return utilisateurRepository
                .findByEmail(username)
                .map((Utilisateur utilisateur) -> new SecurityUser(utilisateurMapper.utilisateurToUtilisateurDto(utilisateur)))
                .orElseThrow(() -> new UsernameNotFoundException("Username not found " + username));
    }
}
