package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.CommandeClientDto;
import com.romand.gestiondestockV1.entities.LigneCommandeClient;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.CommandeClientMapper;
import com.romand.gestiondestockV1.repositories.CommandeClientRepository;
import com.romand.gestiondestockV1.repositories.LigneCommandeClientRepository;
import com.romand.gestiondestockV1.services.CommandeClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class CommandeClientServiceImpl implements CommandeClientService {
    private final CommandeClientRepository commandeClientRepository;
    private final LigneCommandeClientRepository ligneCommandeClientRepository;

    private final CommandeClientMapper commandeClientMapper;

    public CommandeClientServiceImpl(CommandeClientRepository commandeClientRepository,
                                     LigneCommandeClientRepository ligneCommandeClientRepository,
                                     CommandeClientMapper commandeClientMapper) {
        this.commandeClientRepository = commandeClientRepository;
        this.ligneCommandeClientRepository = ligneCommandeClientRepository;
        this.commandeClientMapper = commandeClientMapper;
    }

    @Override
    public CommandeClientDto getCommandeClientById(String idCommandeClient) {
        return commandeClientRepository.findById(idCommandeClient)
                .map(commandeClientMapper::commandeClientToCommandeClientDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucune commande client avec l'ID "+idCommandeClient+" n'a été trouvé dans la BDD",
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)
        );
    }
    @Override
    public Page<CommandeClientDto> getAllCommandesClient(int page, int size) {
        return commandeClientRepository.findAll(PageRequest.of(page, size))
                .map(commandeClientMapper::commandeClientToCommandeClientDto);
    }
    @Override
    public Page<CommandeClientDto> getAllCommandesClientByIdClient(String idClient, int page, int size) {
        return commandeClientRepository.findByClientIdClient(idClient, PageRequest.of(page, size))
                .map(commandeClientMapper::commandeClientToCommandeClientDto);
    }
    @Override
    public CommandeClientDto createCommandeClient(CommandeClientDto commandeClientDto) {
        return commandeClientMapper.commandeClientToCommandeClientDto(commandeClientRepository
                .save(commandeClientMapper.commandeClientDtoToCommandeClient(commandeClientDto)));
    }
    @Override
    public CommandeClientDto updateCommandeClient(String idCommandeClient, CommandeClientDto commandeClientDto) {
        var commandeClient = commandeClientMapper
                .commandeClientDtoToCommandeClient(getCommandeClientById(idCommandeClient));
        commandeClient = commandeClientMapper.commandeClientDtoToCommandeClient(commandeClientDto);
        commandeClient.setIdCommandeClient(idCommandeClient);
        return createCommandeClient(commandeClientMapper.commandeClientToCommandeClientDto(commandeClient));
    }
    @Override
    public String deleteCommandeClient(String idCommandeClient) {
        List<LigneCommandeClient> lignesCommandeClient =
                ligneCommandeClientRepository.findByCommandeClientIdCommandeClient(idCommandeClient);
        if (!lignesCommandeClient.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une commande présente dans une ligne de commande",
                    ErrorCodes.COMMANDE_CLIENT_ALREADY_IN_USE);
        }
        commandeClientRepository.delete(commandeClientMapper
                .commandeClientDtoToCommandeClient(getCommandeClientById(idCommandeClient)));
        return "La commande client a bien été supprimé";
    }
}
