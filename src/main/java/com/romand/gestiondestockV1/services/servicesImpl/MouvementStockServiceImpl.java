package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.MouvementStockDto;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.mappers.MouvementStockMapper;
import com.romand.gestiondestockV1.repositories.MouvementStockRepository;
import com.romand.gestiondestockV1.services.MouvementStockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MouvementStockServiceImpl implements MouvementStockService {
    private final MouvementStockRepository mouvementStockRepository;

    private final MouvementStockMapper mouvementStockMapper;
    public MouvementStockServiceImpl(MouvementStockRepository mouvementStockRepository,
                                     MouvementStockMapper mouvementStockMapper) {
        this.mouvementStockRepository = mouvementStockRepository;
        this.mouvementStockMapper = mouvementStockMapper;
    }

    @Override
    public MouvementStockDto getMouvementStockById(String idMouvementStock) {
        return mouvementStockRepository.findById(idMouvementStock)
                .map(mouvementStockMapper::mouvementStockToMouvementStockDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucun mouvement de stock avec l'ID "+idMouvementStock+" n'a été trouvé dans la BDD",
                        ErrorCodes.MVT_STK_NOT_FOUND));
    }
    @Override
    public Page<MouvementStockDto> getAllMouvementsStock(int page, int size) {
        return mouvementStockRepository.findAll(PageRequest.of(page, size))
                .map(mouvementStockMapper::mouvementStockToMouvementStockDto);
    }
    @Override
    public Page<MouvementStockDto> getAllMouvementsStockByIdArticle(String idArticle, int page, int size) {
        return mouvementStockRepository.findByArticleIdArticle(
                idArticle, PageRequest.of(page, size)).map(mouvementStockMapper::mouvementStockToMouvementStockDto);
    }
    @Override
    public MouvementStockDto createMouvementStock(MouvementStockDto mouvementStockDto) {
        return mouvementStockMapper.mouvementStockToMouvementStockDto(mouvementStockRepository
                .save(mouvementStockMapper.mouvementStockDtoToMouvementStock(mouvementStockDto)));
    }
    @Override
    public MouvementStockDto updateMouvementStock(String idMouvementStock, MouvementStockDto mouvementStockDto) {
        var mouvement = mouvementStockMapper
                .mouvementStockDtoToMouvementStock(getMouvementStockById(idMouvementStock));
        mouvement = mouvementStockMapper.mouvementStockDtoToMouvementStock(mouvementStockDto);
        mouvement.setIdMouvementStock(idMouvementStock);
        return createMouvementStock(mouvementStockMapper.mouvementStockToMouvementStockDto(mouvement));
    }
    @Override
    public String deleteMouvementStockById(String idMouvementStock) {
        mouvementStockRepository.delete(mouvementStockMapper
                .mouvementStockDtoToMouvementStock(getMouvementStockById(idMouvementStock)));
        return "Le mouvement de stock a bien été supprimé";
    }
}
