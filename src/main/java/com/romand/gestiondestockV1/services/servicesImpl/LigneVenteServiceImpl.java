package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.LigneVenteDto;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.mappers.LigneVenteMapper;
import com.romand.gestiondestockV1.repositories.LigneVenteRepository;
import com.romand.gestiondestockV1.services.LigneVenteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LigneVenteServiceImpl implements LigneVenteService {
    private final LigneVenteRepository ligneVenteRepository;

    private final LigneVenteMapper ligneVenteMapper;
    public LigneVenteServiceImpl(LigneVenteRepository ligneVenteRepository, LigneVenteMapper ligneVenteMapper) {
        this.ligneVenteRepository = ligneVenteRepository;
        this.ligneVenteMapper = ligneVenteMapper;
    }

    @Override
    public LigneVenteDto getLigneVenteById(String idLigneVente) {
        return ligneVenteRepository.findById(idLigneVente).map(ligneVenteMapper::ligneVenteToLigneVenteDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucune ligne vente avec l'ID "+idLigneVente+" n'a été trouvée dans la BDD",
                        ErrorCodes.LIGNE_VENTE_NOT_FOUND));
    }
    @Override
    public Page<LigneVenteDto> getAllLignesVente(int page, int size) {
        return ligneVenteRepository.findAll(PageRequest.of(page, size)).map(ligneVenteMapper::ligneVenteToLigneVenteDto);
    }
    @Override
    public Page<LigneVenteDto> getAllLignesVenteByIdArticle(String idArticle, int page, int size) {
        return ligneVenteRepository.findByArticleIdArticle(
                idArticle, PageRequest.of(page, size)).map(ligneVenteMapper::ligneVenteToLigneVenteDto);
    }
    @Override
    public Page<LigneVenteDto> getAllLignesVenteByIdVente(String idVente, int page, int size) {
        return ligneVenteRepository.findByVenteIdVente(
                idVente, PageRequest.of(page, size)).map(ligneVenteMapper::ligneVenteToLigneVenteDto);
    }
    @Override
    public LigneVenteDto createLigneVente(LigneVenteDto ligneVenteDto) {
        return ligneVenteMapper.ligneVenteToLigneVenteDto(ligneVenteRepository
                .save(ligneVenteMapper.ligneVenteDtoToLigneVente(ligneVenteDto)));
    }
    @Override
    public LigneVenteDto updateLigneVente(String idLigneVente, LigneVenteDto ligneVenteDto) {
        var ligneVente = ligneVenteMapper.ligneVenteDtoToLigneVente(getLigneVenteById(idLigneVente));
        ligneVente = ligneVenteMapper.ligneVenteDtoToLigneVente(ligneVenteDto);
        ligneVente.setIdLigneVente(idLigneVente);
        return createLigneVente(ligneVenteMapper.ligneVenteToLigneVenteDto(ligneVente));
    }
    @Override
    public String deleteLigneVenteById(String idLigneVente) {
        ligneVenteRepository.delete(ligneVenteMapper.ligneVenteDtoToLigneVente(getLigneVenteById(idLigneVente)));
        return "La ligne vente a bien été supprimée";
    }
}
