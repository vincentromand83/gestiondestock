package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.LigneCommandeFournisseurDto;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.mappers.LigneCommandeFournisseurMapper;
import com.romand.gestiondestockV1.repositories.LigneCommandeFournisseurRepository;
import com.romand.gestiondestockV1.services.LigneCommandeFournisseurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LigneCommandeFournisseurServiceImpl implements LigneCommandeFournisseurService {
    private final LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository;

    private final LigneCommandeFournisseurMapper ligneCommandeFournisseurMapper;

    public LigneCommandeFournisseurServiceImpl(LigneCommandeFournisseurRepository ligneCommandeFournisseurRepository,
                                               LigneCommandeFournisseurMapper ligneCommandeFournisseurMapper) {
        this.ligneCommandeFournisseurRepository = ligneCommandeFournisseurRepository;
        this.ligneCommandeFournisseurMapper = ligneCommandeFournisseurMapper;
    }

    @Override
    public LigneCommandeFournisseurDto getLigneCommandeFournisseurById(String idLigneCommandeFournisseur) {
        return ligneCommandeFournisseurRepository.findById(idLigneCommandeFournisseur)
                .map(ligneCommandeFournisseurMapper::ligneCommandeFournisseurToLigneCommandeFournisseurDto).orElseThrow(
                        ()-> new EntityNotFoundException(
                        "Aucune ligne commande fournisseur avec l'ID"+idLigneCommandeFournisseur+" n'a été trouvé en BDD",
                        ErrorCodes.LIGNE_COMMANDE_FOURNISSEUR_NOT_FOUND));
    }
    @Override
    public Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseur(int page, int size) {
        return ligneCommandeFournisseurRepository.findAll(PageRequest.of(page, size))
                .map(ligneCommandeFournisseurMapper::ligneCommandeFournisseurToLigneCommandeFournisseurDto);
    }
    @Override
    public Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseurByIdArticle(String idArticle, int page, int size) {
        return ligneCommandeFournisseurRepository.findByArticleIdArticle(idArticle, PageRequest.of(page, size))
                .map(ligneCommandeFournisseurMapper::ligneCommandeFournisseurToLigneCommandeFournisseurDto);
    }
    @Override
    public Page<LigneCommandeFournisseurDto> getAllLignesCommandeFournisseurByIdCommandeFournisseur(
            String idCommandeFournisseur, int page, int size) {
        return ligneCommandeFournisseurRepository.findByCommandeFournisseurIdCommandeFournisseur(
                idCommandeFournisseur, PageRequest.of(page, size))
                .map(ligneCommandeFournisseurMapper::ligneCommandeFournisseurToLigneCommandeFournisseurDto);
    }
    @Override
    public LigneCommandeFournisseurDto createLigneCommandeFournisseur(LigneCommandeFournisseurDto ligneCommandeFournisseurDto) {
        return ligneCommandeFournisseurMapper.ligneCommandeFournisseurToLigneCommandeFournisseurDto(
                ligneCommandeFournisseurRepository.save(ligneCommandeFournisseurMapper
                        .ligneCommandeFournisseurDtoToLigneCommandeFournisseur(ligneCommandeFournisseurDto)));
    }
    @Override
    public LigneCommandeFournisseurDto updateLigneCommandeFournisseur(
            String idLigneCommandeFournisseur, LigneCommandeFournisseurDto ligneCommandeFournisseurDto) {
        var lcf = ligneCommandeFournisseurMapper.ligneCommandeFournisseurDtoToLigneCommandeFournisseur(
                getLigneCommandeFournisseurById(idLigneCommandeFournisseur));
        lcf = ligneCommandeFournisseurMapper.ligneCommandeFournisseurDtoToLigneCommandeFournisseur(ligneCommandeFournisseurDto);
        lcf.setIdLigneCommandeFournisseur(idLigneCommandeFournisseur);
        return createLigneCommandeFournisseur(
                ligneCommandeFournisseurMapper.ligneCommandeFournisseurToLigneCommandeFournisseurDto(lcf));
    }
    @Override
    public String deleteLigneCommandeFournisseur(String idLigneCommandeFournisseur) {
        ligneCommandeFournisseurRepository.delete(ligneCommandeFournisseurMapper.ligneCommandeFournisseurDtoToLigneCommandeFournisseur(
                getLigneCommandeFournisseurById(idLigneCommandeFournisseur)));
        return "La ligne commande fournisseur a bien été supprimée";
    }
}
