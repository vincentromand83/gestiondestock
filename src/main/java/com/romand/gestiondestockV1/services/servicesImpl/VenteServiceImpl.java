package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.VenteDto;
import com.romand.gestiondestockV1.entities.LigneVente;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.VenteMapper;
import com.romand.gestiondestockV1.repositories.LigneVenteRepository;
import com.romand.gestiondestockV1.repositories.VenteRepository;
import com.romand.gestiondestockV1.services.VenteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class VenteServiceImpl implements VenteService {

    private final VenteRepository venteRepository;
    private final LigneVenteRepository ligneVenteRepository;

    private final VenteMapper venteMapper;

    public VenteServiceImpl(VenteRepository venteRepository, LigneVenteRepository ligneVenteRepository,
                            VenteMapper venteMapper) {
        this.venteRepository = venteRepository;
        this.ligneVenteRepository = ligneVenteRepository;
        this.venteMapper = venteMapper;
    }

    @Override
    public VenteDto getVenteById(String idVente) {
        return venteRepository.findById(idVente).map(venteMapper::venteToVenteDto).orElseThrow(()->
                new EntityNotFoundException("Aucune vente avec l'ID "+idVente+" n'a été trouvé dans la BDD",
                        ErrorCodes.VENTE_NOT_FOUND));
    }
    @Override
    public Page<VenteDto> getAllVentes(int page, int size) {
        return venteRepository.findAll(PageRequest.of(page, size)).map(venteMapper::venteToVenteDto);
    }
    @Override
    public VenteDto createVente(VenteDto venteDto) {
        return venteMapper.venteToVenteDto(venteRepository.save(venteMapper.venteDtoToVente(venteDto)));
    }
    @Override
    public VenteDto updateVente(String idVente, VenteDto venteDto) {
        var vente = venteMapper.venteDtoToVente(getVenteById(idVente));
        vente = venteMapper.venteDtoToVente(venteDto);
        vente.setIdVente(idVente);
        return createVente(venteMapper.venteToVenteDto(vente));
    }
    @Override
    public String deleteVenteById(String idVente) {
        List<LigneVente> lignesVente = ligneVenteRepository.findByVenteIdVente(idVente);
        if (!lignesVente.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une vente présente dans une ligne de vente",
                    ErrorCodes.VENTE_ALREADY_IN_USE);
        }
        venteRepository.delete(venteMapper.venteDtoToVente(getVenteById(idVente)));
        return "La vente a bien été supprimée";
    }
}
