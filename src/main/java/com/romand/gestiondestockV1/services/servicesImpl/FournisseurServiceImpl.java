package com.romand.gestiondestockV1.services.servicesImpl;

import com.romand.gestiondestockV1.dtos.FournisseurDto;
import com.romand.gestiondestockV1.entities.CommandeFournisseur;
import com.romand.gestiondestockV1.exceptions.EntityNotFoundException;
import com.romand.gestiondestockV1.exceptions.ErrorCodes;
import com.romand.gestiondestockV1.exceptions.InvalidOperationException;
import com.romand.gestiondestockV1.mappers.FournisseurMapper;
import com.romand.gestiondestockV1.repositories.CommandeFournisseurRepository;
import com.romand.gestiondestockV1.repositories.FournisseurRepository;
import com.romand.gestiondestockV1.services.FournisseurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class FournisseurServiceImpl implements FournisseurService {
    private final FournisseurRepository fournisseurRepository;
    private final CommandeFournisseurRepository commandeFournisseurRepository;

    private final FournisseurMapper fournisseurMapper;

    public FournisseurServiceImpl(FournisseurRepository fournisseurRepository,
                                  CommandeFournisseurRepository commandeFournisseurRepository,
                                  FournisseurMapper fournisseurMapper) {
        this.fournisseurRepository = fournisseurRepository;
        this.commandeFournisseurRepository = commandeFournisseurRepository;
        this.fournisseurMapper = fournisseurMapper;
    }

    @Override
    public FournisseurDto getFournisseurById(String idFournisseur) {
        return fournisseurRepository.findById(idFournisseur)
                .map(fournisseurMapper::fournisseurToFournisseurDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucun fournisseur avec l'ID "+idFournisseur+" n'a été trouvé dans la BDD",
                        ErrorCodes.FOURNISSEUR_NOT_FOUND));
    }
    @Override
    public Page<FournisseurDto> getAllFournisseurs(int page, int size) {
        return fournisseurRepository.findAll(PageRequest.of(page, size)).map(fournisseurMapper::fournisseurToFournisseurDto);
    }
    @Override
    public FournisseurDto createFournisseur(FournisseurDto fournisseurDto) {
        return fournisseurMapper.fournisseurToFournisseurDto(fournisseurRepository.save(
                fournisseurMapper.fournisseurDtoToFournisseur(fournisseurDto)));
    }
    @Override
    public FournisseurDto updateFournisseur(String idFournisseur, FournisseurDto fournisseurDto) {
        var fournisseur = fournisseurMapper.fournisseurDtoToFournisseur(getFournisseurById(idFournisseur));
        fournisseur = fournisseurMapper.fournisseurDtoToFournisseur(fournisseurDto);
        fournisseur.setIdFournisseur(idFournisseur);
        return createFournisseur(fournisseurMapper.fournisseurToFournisseurDto(fournisseur));
    }
    @Override
    public String deleteFournisseurById(String idFournisseur) {
        List<CommandeFournisseur> fournisseurs = commandeFournisseurRepository
                .findByFournisseurIdFournisseur(idFournisseur);
        if (!fournisseurs.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer un fournisseur présent dans une commande fournisseur",
                    ErrorCodes.FOURNISSEUR_ALREADY_IN_USE);
        }
        fournisseurRepository.delete(fournisseurMapper.fournisseurDtoToFournisseur(getFournisseurById(idFournisseur)));
        return "Le fournisseur a bien été supprimé";
    }
}
